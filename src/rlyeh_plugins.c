/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <dirent.h>
#include <debug/sahtrace.h>
#include <amxc/amxc_variant.h>
#include <amxm/amxm.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_dm.h>

#include <rlyeh/rlyeh_defines.h>
#include <rlyeh_priv.h>
#include <rlyeh_plugins_priv.h>
#include <rlyeh/rlyeh_plugins.h>

#define ME "rlyeh_plugin"

typedef void (* plugin_handler) (const char* const plugin, const char* const command, amxc_var_t* ret);
static const amxd_dm_t* datamodel = NULL;
static bool auth_hook_registered = false;

static amxc_llist_t plugins;

typedef struct _plugin_data {
    char* so_file;
    amxm_shared_object_t* plugin;
    amxc_llist_it_t lit;
} plugin_data_t;


static int plugin_data_init(plugin_data_t* const data) {
    int retval = -1;
    ASSERT_NOT_NULL(data, goto exit);
    ASSERT_SUCCESS(amxc_llist_it_init(&data->lit), goto exit, "Failed to initialize plugin");
    data->so_file = NULL;
    data->plugin = NULL;

    retval = 0;
exit:
    return retval;
}

static void plugin_data_clean(plugin_data_t* const data) {
    ASSERT_NOT_NULL(data, goto exit);
    free_null(data->so_file);
    if(data->plugin) {
        amxm_so_close(&data->plugin);
        data->plugin = NULL;
    }
exit:
    return;
}

static void default_handler(const char* const plugin,
                            const char* const command, amxc_var_t* ret) {
    int res = amxc_var_dyncast(int32_t, ret);
    CHECK_NOT_EQUAL_LOG(INFO, res, 0, NO_INSTRUCTION,
                        "[%s] command [%s] returned %d", plugin, command, res);
}

static int plugin_data_new(plugin_data_t** data) {
    int retval = -1;
    ASSERT_NOT_NULL(data, goto exit);

    *data = (plugin_data_t*) calloc(1, sizeof(plugin_data_t));
    ASSERT_NOT_NULL(*data, goto exit);

    ASSERT_TRUE(plugin_data_init(*data) >= 0, goto exit, "Failed to init data");
    retval = 0;

exit:
    return retval;
}

static void plugin_data_delete(plugin_data_t** data) {
    ASSERT_NOT_NULL(data, goto exit);
    plugin_data_clean(*data);

    free_null(*data);
exit:
    return;
}

static void plugin_data_it_free(amxc_llist_it_t* it) {
    plugin_data_t* data = amxc_llist_it_get_data(it, plugin_data_t, lit);
    plugin_data_delete(&data);
}

static void plugin_exec(const char* command, amxc_var_t* args, plugin_handler handler, int* status) {
    plugin_data_t* data = NULL;
    if(status) {
        *status = -1;
    }
    amxc_llist_for_each(it, (&plugins)) {
        amxc_var_t ret;
        amxc_var_init(&ret);

        data = amxc_llist_it_get_data(it, plugin_data_t, lit);
        if(!amxm_so_has_function(data->plugin, RLYEH_PLUGIN, command)) {
            continue;
        }
        if(amxm_so_execute_function(data->plugin, RLYEH_PLUGIN, command, args, &ret) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to execute %s on %s", command, data->so_file);
        }
        if(handler) {
            handler(data->so_file, command, &ret);
        }
        if(status) {
            *status = amxc_var_dyncast(int32_t, &ret);
        }
        amxc_var_clean(&ret);
    }
}

void plugin_init(void) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_new_key_data(&args, RLYEH_PLUGIN_ARG_DATAMODEL, (void*) datamodel);

    plugin_exec(RLYEH_PLUGIN_INIT, &args, default_handler, NULL);
    amxc_var_clean(&args);
}

void plugin_cleanup(void) {
    amxc_var_t args;
    amxc_var_init(&args);
    plugin_exec(RLYEH_PLUGIN_CLEANUP, &args, default_handler, NULL);
    amxc_var_clean(&args);
}

int plugin_image_authentication(const rlyeh_signature_data_t* sigdata) {
    int status = -1;
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, RLYEH_PLUGIN_ARG_URL, sigdata->signature_url_overload);
    amxc_var_add_key(cstring_t, &args, RLYEH_PLUGIN_ARG_MANIFEST_HASH, sigdata->manifest_hash);
    amxc_var_add_key(cstring_t, &args, RLYEH_PLUGIN_ARG_IMAGE_NAME, sigdata->image_name);
    amxc_var_add_key(cstring_t, &args, RLYEH_PLUGIN_ARG_SERVER, sigdata->server);
    amxc_var_add_key(cstring_t, &args, RLYEH_PLUGIN_ARG_VERSION, sigdata->version);

    plugin_exec(RLYEH_PLUGIN_IMAGE_AUTHENTICATION, &args, default_handler, &status);
    amxc_var_clean(&args);

    return status;
}

static int plugin_load(const char* plugin_so, const char* plugin_fullpath) {
    int res = -1;
    plugin_data_t* plugin_data = NULL;
    SAH_TRACEZ_INFO(ME, "Load plugin [%s]", plugin_so);
    if(plugin_data_new(&plugin_data) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create plugin_data");
        goto error;
    }
    plugin_data->so_file = strdup(plugin_so);

    ASSERT_SUCCESS(amxm_so_open(&plugin_data->plugin, plugin_so, plugin_fullpath), goto error,
                   "Can not load plugin %s", plugin_so);

    // Only a single plugin can register authentication hook
    if(amxm_so_has_function(plugin_data->plugin, RLYEH_PLUGIN, RLYEH_PLUGIN_IMAGE_AUTHENTICATION)) {
        if(auth_hook_registered == true) {
            SAH_TRACEZ_ERROR(ME, "An authentication hook is already registered. \
                Ignorning plugin %s", plugin_so);
            amxm_so_close(&plugin_data->plugin);
            goto error;
        } else {
            auth_hook_registered = true;
        }
    }

    if(amxm_so_get_module(plugin_data->plugin, RLYEH_PLUGIN) == NULL) {
        SAH_TRACEZ_ERROR(ME, "Library does not provide %s", RLYEH_PLUGIN);
        amxm_so_close(&plugin_data->plugin);
        plugin_data->plugin = NULL;
        goto error;
    }
    amxc_llist_append(&plugins, &plugin_data->lit);

    res = 0;
    goto exit;
error:
    plugin_data_delete(&plugin_data);
exit:
    return res;
}

int rlyeh_plugins_init(const amxd_dm_t* dm, const char* plugin_location) {
    int res = -1;
    char plugin_path[PATH_MAX] = {};
    DIR* plugin_dir = NULL;
    struct dirent* entry;

    if(amxc_llist_init(&plugins) < 0) {
        goto exit;
    }

    // dm is allowed to be NULL to allow standalone app to use
    // librlyeh without necessairly implementing a datamodel.
    // Plugins might not allow properly functionniong without datamodel access
    CHECK_NULL_LOG(WARNING, dm, NO_INSTRUCTION,
                   "DM parameter NULL, plugins might not behavior correctly");
    ASSERT_STR_NOT_EMPTY(plugin_location, return res, "No plugin location is defined");
    datamodel = dm;

    ASSERT_EQUAL(lcm_filetype(plugin_location), FILE_TYPE_DIRECTORY,
                 goto exit, "Plugin dir [%s] does not exist", plugin_location);

    ASSERT_NOT_NULL((plugin_dir = opendir(plugin_location)), goto exit,
                    "Plugin dir [%s] can not be opened", plugin_location);

    while((entry = readdir(plugin_dir))) {
        CHECK_FALSE_LOG(ERROR, snprintf(plugin_path, sizeof(plugin_path), "%s/%s",
                                        plugin_location, entry->d_name) < (int) sizeof(plugin_path),
                        continue, "Plugin [%s] name to long. Ignored", entry->d_name);
        if(lcm_filetype(plugin_path) == FILE_TYPE_REGULAR) {
            plugin_load(plugin_path, plugin_path);
        }
    }

    // Init all plugins
    plugin_init();

    res = 0;
exit:
    if(plugin_dir) {
        closedir(plugin_dir);
    }
    return res;
}

void rlyeh_plugins_cleanup(void) {
    // send cleanup command to all plugins
    plugin_cleanup();

    amxc_llist_clean(&plugins, plugin_data_it_free);
}
