/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>

#include <amxc/amxc.h>
#include <debug/sahtrace.h>

#include <rlyeh/rlyeh_utils.h>
#include <rlyeh/rlyeh_images.h>

#include "rlyeh_priv.h"
#include "rlyeh_defines_priv.h"

#include <lcm/lcm_assert.h>

#define ME "rlyeh_images"

void rlyeh_imagespec_manifest_element_init(rlyeh_imagespec_manifest_element_t* data) {
    memset((void*) data, 0, sizeof(rlyeh_imagespec_manifest_element_t));
    amxc_llist_it_init(&data->it);
}

void rlyeh_imagespec_index_init(rlyeh_imagespec_index_t* data) {
    data->image_disk_location = NULL;
    amxc_llist_init(&data->manifest_elements);
    amxc_llist_it_init(&data->it);
}

void rlyeh_imagespec_manifest_element_clean(rlyeh_imagespec_manifest_element_t* item) {
    free(item->version);
    free(item->duid);
    free(item->name);
    free(item->vendor);
    free(item->uri);
    free(item->description);
    free(item->digest);
    amxc_llist_it_clean(&item->it, NULL);
}

static void imagespec_manifest_element_it_free(amxc_llist_it_t* it) {
    rlyeh_imagespec_manifest_element_t* item = amxc_llist_it_get_data(it, rlyeh_imagespec_manifest_element_t, it);
    rlyeh_imagespec_manifest_element_clean(item);
    free(item);
}

static void imagespec_index_it_free(amxc_llist_it_t* it) {
    rlyeh_imagespec_index_t* index = amxc_llist_it_get_data(it, rlyeh_imagespec_index_t, it);
    rlyeh_imagespec_index_clean(index);
    free(index);
}

void rlyeh_imagespec_index_clean(rlyeh_imagespec_index_t* index) {
    free(index->image_disk_location);
    amxc_llist_clean(&index->manifest_elements, imagespec_manifest_element_it_free);
    amxc_llist_it_clean(&index->it, NULL);
}

void fill_imagespec_manifest_element_data_from_manifest_schema(const image_spec_schema_image_manifest_schema* image_manifest, rlyeh_imagespec_manifest_element_t* item) {
    if(image_manifest->annotations) {
        for(size_t index_annotations = 0; index_annotations < image_manifest->annotations->len; index_annotations++) {
            const char* key = image_manifest->annotations->keys[index_annotations];
            const char* value = image_manifest->annotations->values[index_annotations];
            if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_TITLE, key) == 0) {
                free(item->name);
                item->name = strdup(value);
            }
            if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_REFNAME, key) == 0) {
                free(item->version);
                item->version = strdup(value);
            } else if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_VENDOR, key) == 0) {
                free(item->vendor);
                item->vendor = strdup(value);
            } else if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_DESCRIPTION, key) == 0) {
                free(item->description);
                item->description = strdup(value);
            }
        }
    }
}

static void fill_imagespec_manifest_element_data_from_manifest(image_spec_schema_image_index_schema_manifests_element* manifest, rlyeh_imagespec_manifest_element_t* item) {
    if(manifest->annotations) {
        for(size_t index_annotations = 0; index_annotations < manifest->annotations->len; index_annotations++) {
            const char* key = manifest->annotations->keys[index_annotations];
            const char* value = manifest->annotations->values[index_annotations];
            if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_TITLE, key) == 0) {
                item->name = strdup(value);
            } else if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_REFNAME, key) == 0) {
                item->version = strdup(value);
            } else if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_VENDOR, key) == 0) {
                item->vendor = strdup(value);
            } else if(strcmp(RLYEH_ANNOTATION_LCM_DUID, key) == 0) {
                item->duid = strdup(value);
            } else if(strcmp(RLYEH_ANNOTATION_LCM_URI, key) == 0) {
                item->uri = strdup(value);
            } else if(strcmp(RLYEH_ANNOTATION_LCM_MAKRFORREMOVAL, key) == 0) {
                item->mark_for_removal = atoi(value);
            } else if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_DESCRIPTION, key) == 0) {
                item->description = strdup(value);
            }
        }
    }

    item->manifest_size = manifest->size;

    item->digest = strdup(manifest->digest);
}

typedef struct _args_build_imagespec_list_inner {
    amxc_llist_t* limagespec;
    size_t amount_of_images;
} args_build_imagespec_list_inner_t;

static void build_imagespec_list_inner(const char* image_path, const char* index_path, void* args) {
    args_build_imagespec_list_inner_t* parsed_args = (args_build_imagespec_list_inner_t*) args;
    amxc_llist_t* limagespec = parsed_args->limagespec;
    image_spec_schema_image_index_schema* index_schema = NULL;
    rlyeh_imagespec_index_t* index = (rlyeh_imagespec_index_t*) malloc(sizeof(rlyeh_imagespec_index_t));
    rlyeh_imagespec_index_init(index);

    index->image_disk_location = strdup(image_path);

    index_schema = rlyeh_parse_image_spec_schema(index_path);
    if(index_schema) {
        for(size_t i = 0; i < index_schema->manifests_len; i++) {
            rlyeh_imagespec_manifest_element_t* manifest = (rlyeh_imagespec_manifest_element_t*) malloc(sizeof(rlyeh_imagespec_manifest_element_t));
            rlyeh_imagespec_manifest_element_init(manifest);
            fill_imagespec_manifest_element_data_from_manifest(index_schema->manifests[i], manifest);
            amxc_llist_append(&index->manifest_elements, &(manifest->it));
            parsed_args->amount_of_images++;
        }
        if(!amxc_llist_is_empty(&index->manifest_elements)) {
            amxc_llist_append(limagespec, &(index->it));
        } else {
            amxc_llist_it_clean(&index->it, imagespec_index_it_free);
        }
        free_image_spec_schema_image_index_schema(index_schema);
    } else {
        amxc_llist_it_clean(&index->it, imagespec_index_it_free);
    }
}

int rlyeh_images_list_build(rlyeh_images_list_t* images_list, const char* image_path) {
    int ret = -1;
    args_build_imagespec_list_inner_t args;
    args.limagespec = images_list;
    args.amount_of_images = 0;

    if(images_list == NULL) {
        goto exit;
    }

    if((image_path == NULL) || (image_path[0] == '\0')) {
        goto exit;
    }

    rlyeh_loop_through_index_files(image_path, build_imagespec_list_inner, (void*) &args);
    ret = (int) args.amount_of_images;

exit:
    return ret;
}

void rlyeh_loop_over_images_list(amxc_llist_t* images_list, rlyeh_image_list_callback_t fn, void* args) {
    amxc_llist_for_each(it, images_list) {
        rlyeh_imagespec_index_t* index = amxc_llist_it_get_data(it, rlyeh_imagespec_index_t, it);

        amxc_llist_for_each(subit, &index->manifest_elements) {
            rlyeh_imagespec_manifest_element_t* manifest = amxc_llist_it_get_data(subit, rlyeh_imagespec_manifest_element_t, it);
            fn(index, manifest, args);
        }

        if(amxc_llist_is_empty(&index->manifest_elements)) {
            // if no items are left in the list, delete the imagespec from the list
            amxc_llist_it_clean(it, imagespec_index_it_free);
        }
    }
}

void rlyeh_images_list_init(rlyeh_images_list_t* images_list) {
    amxc_llist_init(images_list);
}

void rlyeh_images_list_clean(rlyeh_images_list_t* images_list) {
    amxc_llist_clean(images_list, imagespec_index_it_free);
}

void rlyeh_remove_manifest_element_from_index(rlyeh_imagespec_manifest_element_t* manifest) {
    amxc_llist_it_clean(&manifest->it, imagespec_manifest_element_it_free);
}

int rlyeh_image_index_file_build_from_manifest(const char* image_location, const rlyeh_imagespec_manifest_element_t* manifest) {
    int ret = -1;
    image_spec_schema_image_index_schema* index_schema = NULL;
    image_spec_schema_image_index_schema_manifests_element* manifest_ele = NULL;
    image_spec_schema_image_index_schema_manifests_element** ptr_manifests = NULL;
    amxc_string_t index_filename;

    amxc_string_init(&index_filename, 0);
    amxc_string_setf(&index_filename, "%s/index.json", image_location);

    index_schema = rlyeh_parse_image_spec_schema(index_filename.buffer);
    if(index_schema) {
        size_t it = 0;
        ret = rlyeh_get_it_from_index(&it, index_schema, manifest->version, manifest->duid);
        if(ret == 0) {
            manifest_ele = index_schema->manifests[it];
            image_spec_schema_image_index_manifest_element_clean(manifest_ele);
        } else {
            // We need to extend an existing manifest_element list in the index
            size_t old_manifest_len = index_schema->manifests_len;
            index_schema->manifests_len++;
            ptr_manifests = \
                (image_spec_schema_image_index_schema_manifests_element**) calloc(index_schema->manifests_len, sizeof(*ptr_manifests));
            manifest_ele = (image_spec_schema_image_index_schema_manifests_element*) calloc(1, sizeof(*manifest_ele));
            for(size_t i = 0; i < old_manifest_len; i++) {
                ptr_manifests[i] = index_schema->manifests[i];
            }
            ptr_manifests[old_manifest_len] = manifest_ele;
            free(index_schema->manifests);
            index_schema->manifests = ptr_manifests;
        }
    } else {
        // create a completely new index
        index_schema = (image_spec_schema_image_index_schema*) calloc(1, sizeof(*index_schema));
        ptr_manifests = \
            (image_spec_schema_image_index_schema_manifests_element**) calloc(1, sizeof(*ptr_manifests));
        index_schema->manifests = ptr_manifests;
        manifest_ele = (image_spec_schema_image_index_schema_manifests_element*) calloc(1, sizeof(*manifest_ele));
        ptr_manifests[0] = manifest_ele;
        index_schema->schema_version_present = 1;
        index_schema->schema_version = 2;
        index_schema->manifests_len = 1;
    }

    fill_manifest_element_from_manifest(manifest_ele, manifest);
    ret = rlyeh_write_index_file_from_image_spec_schema(index_filename.buffer, index_schema);
    if(ret < 1) {
        SAH_TRACEZ_ERROR(ME, "Cannot write the generated index '%s'", index_filename.buffer);
    }

    free_image_spec_schema_image_index_schema(index_schema);
    amxc_string_clean(&index_filename);
    return ret;
}

void fill_manifest_element_from_manifest(image_spec_schema_image_index_schema_manifests_element* manifest_to_fill, const rlyeh_imagespec_manifest_element_t* manifest) {
    json_map_string_string* annotations = (json_map_string_string*) calloc(1, sizeof(json_map_string_string));

    append_json_map_string_string(annotations, RLYEH_ANNOTATION_LCM_DUID, manifest->duid);
    append_json_map_string_string(annotations, RLYEH_ANNOTATION_OCI_IMAGE_REFNAME, manifest->version);
    append_json_map_string_string(annotations, RLYEH_ANNOTATION_OCI_IMAGE_TITLE, manifest->name ? manifest->name : "");
    append_json_map_string_string(annotations, RLYEH_ANNOTATION_OCI_IMAGE_VENDOR, manifest->vendor ? manifest->vendor : "");
    append_json_map_string_string(annotations, RLYEH_ANNOTATION_OCI_IMAGE_DESCRIPTION, manifest->description ? manifest->description : "");
    append_json_map_string_string(annotations, RLYEH_ANNOTATION_LCM_URI, manifest->uri);
    append_json_map_string_string(annotations, RLYEH_ANNOTATION_LCM_MAKRFORREMOVAL, manifest->mark_for_removal ? "1" : "0");
    manifest_to_fill->annotations = annotations;

    if(strncmp(manifest->digest, "sha256:", 7) == 0) {
        manifest_to_fill->digest = strdup(manifest->digest);
    } else {
        amxc_string_t newdigest;
        amxc_string_init(&newdigest, 0);
        amxc_string_setf(&newdigest, "sha256:%s", manifest->digest);
        manifest_to_fill->digest = strdup(newdigest.buffer);
        amxc_string_clean(&newdigest);
    }

    manifest_to_fill->media_type = strdup("application/vnd.oci.image.manifest.v1+json");
    manifest_to_fill->size_present = 1;
    manifest_to_fill->size = manifest->manifest_size;
    manifest_to_fill->urls_len = 0;

    return;
}

void image_spec_schema_image_index_manifest_element_clean(image_spec_schema_image_index_schema_manifests_element* manifest) {
    if(manifest->annotations) {
        free_json_map_string_string(manifest->annotations);
    }
    free(manifest->digest);
    free(manifest->media_type);
    memset((void*) manifest, 0, sizeof(image_spec_schema_image_index_schema_manifests_element));
}

int rlyeh_update_index_annotations(const char* disk_location,
                                   const char* duid,
                                   const char* version,
                                   const char* annotation_key,
                                   const char* annotation_value) {
    int ret = -1;
    image_spec_schema_image_index_schema* index_schema = NULL;
    size_t it;
    amxc_string_t index_path;
    amxc_string_init(&index_path, 0);
    amxc_string_setf(&index_path, "%s/index.json", disk_location);

    index_schema = rlyeh_parse_image_spec_schema(index_path.buffer);
    if(index_schema == NULL) {
        goto exit;
    }

    ret = rlyeh_get_it_from_index(&it, index_schema, version, duid);
    if(ret == 0) {
        rlyeh_update_annotation_in_manifest_element(index_schema->manifests[it], annotation_key, annotation_value);
        ret = rlyeh_write_index_file_from_image_spec_schema(index_path.buffer, index_schema);
        if(ret < 1) {
            SAH_TRACEZ_ERROR(ME, "Couldnt write the new %s for the annotation update [%s:%s]", index_path.buffer, annotation_key, annotation_value);
        }
    }

exit:
    free_image_spec_schema_image_index_schema(index_schema);
    amxc_string_clean(&index_path);
    return ret;
}

int rlyeh_imagespec_manifest_element_get_from_annotations(const char* disk_location,
                                                          const char* storage_location,
                                                          const char* duid,
                                                          const char* version,
                                                          rlyeh_imagespec_manifest_element_t* manifest) {
    int ret = -1;
    parser_error err = NULL;
    image_spec_schema_image_index_schema* index_schema = NULL;
    amxc_string_t manifest_name;
    amxc_string_t index_path;
    amxc_string_init(&index_path, 0);
    amxc_string_setf(&index_path, "%s/index.json", disk_location);

    index_schema = rlyeh_parse_image_spec_schema(index_path.buffer);
    if(!index_schema) {
        SAH_TRACEZ_ERROR(ME, "Index schema not found [%s]", index_path.buffer);
        goto exit;
    }

    amxc_string_init(&manifest_name, 0);
    ret = build_manifest_filename_from_index(index_schema, version, duid, storage_location, &manifest_name);
    if(ret) {
        SAH_TRACEZ_ERROR(ME, "Could not build manifest filename from the index [%s]: %d", index_path.buffer, ret);
        goto exit_free_manifest_name;
    }

    image_spec_schema_image_manifest_schema* manifest_schema = NULL;
    manifest_schema = image_spec_schema_image_manifest_schema_parse_file(manifest_name.buffer, 0, &err);
    if(manifest_schema == NULL) {
        SAH_TRACEZ_ERROR(ME, "Manifest schema not found [%s]: %s", manifest_name.buffer, err ? err : "????");
        free(err);
        goto exit_free_manifest_name;
    }

    fill_imagespec_manifest_element_data_from_manifest_schema(manifest_schema, manifest);
    if(manifest->version == NULL) {
        manifest->version = strdup(version);
    }
    manifest->duid = strdup(duid);

    ret = 0;
    free_image_spec_schema_image_manifest_schema(manifest_schema);
exit_free_manifest_name:
    amxc_string_clean(&manifest_name);
    free_image_spec_schema_image_index_schema(index_schema);
exit:
    amxc_string_clean(&index_path);
    return ret;
}

typedef struct save_images_args {
    size_t amountsaved;
    size_t amountfailed;
} save_images_args_t;

static void save_images_inner(rlyeh_imagespec_index_t* index,
                              rlyeh_imagespec_manifest_element_t* manifest,
                              void* args) {
    save_images_args_t* parsed_args = (save_images_args_t*) args;
    amxc_string_t image_location;
    amxc_string_init(&image_location, 0);
    amxc_string_setf(&image_location, "%s/", index->image_disk_location);
    if(rlyeh_image_index_file_build_from_manifest(image_location.buffer, manifest) < 1) {
        SAH_TRACEZ_ERROR(ME, "Cannot save the manifest element to an index: [%s] %s",
                         manifest->duid,
                         image_location.buffer);
        parsed_args->amountfailed++;
    } else {
        parsed_args->amountsaved++;
    }

    amxc_string_clean(&image_location);
}

int rlyeh_save_images_from_list(rlyeh_images_list_t* images_list) {
    save_images_args_t args;
    args.amountsaved = 0;
    args.amountfailed = 0;
    rlyeh_loop_over_images_list(images_list, save_images_inner, &args);
    SAH_TRACEZ_INFO(ME, "Saved %zu images - Failed %zu images", args.amountsaved, args.amountfailed);
    return (int) args.amountfailed;
}

typedef struct remove_index_args {
    size_t amount_removed;
} remove_index_args_t;

static void remove_index_inner(rlyeh_imagespec_index_t* index,
                               rlyeh_imagespec_manifest_element_t* manifest,
                               void* args) {
    (void) manifest;
    remove_index_args_t* parsed_args = (remove_index_args_t*) args;
    amxc_string_t index_path;
    amxc_string_init(&index_path, 0);
    amxc_string_setf(&index_path, "%s/index.json", index->image_disk_location);
    if(rlyeh_remove_file(index_path.buffer) == 0) {
        parsed_args->amount_removed++;
    }
    amxc_string_clean(&index_path);
}

int rlyeh_remove_index_files_from_list(rlyeh_images_list_t* images_list) {
    remove_index_args_t args;
    args.amount_removed = 0;
    rlyeh_loop_over_images_list(images_list, remove_index_inner, &args);
    return (int) args.amount_removed;
}
