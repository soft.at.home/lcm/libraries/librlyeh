/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <errno.h>
#include <string.h>

#include <rlyeh/rlyeh_remove.h>
#include <rlyeh/rlyeh_utils.h>
#include <rlyeh/rlyeh_images.h>
#include <rlyeh/rlyeh_image_oci_layout.h>
#include <rlyeh_priv.h>
#include <rlyeh_defines_priv.h>

#include <debug/sahtrace.h>

#include <libocispec/image_spec_schema_image_index_schema.h>
#include <libocispec/image_spec_schema_image_manifest_schema.h>

#include <lcm/lcm_assert.h>

#define ME "rlyeh_remove"

static void take_used_blobs_from_list_using_manifest(amxc_llist_t* lblobs, const char* storage_path, rlyeh_imagespec_manifest_element_t* manifest) {
    parser_error err = NULL;
    amxc_string_t manifest_blob_path;
    image_spec_schema_image_manifest_schema* manifest_schema = NULL;

    amxc_string_init(&manifest_blob_path, 0);

    if(manifest->digest == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing digest from manifest [%s:%s:%s]", manifest->name, manifest->duid, manifest->version);
        goto exit;
    }

    build_fname_from_digest(storage_path, manifest->digest, &manifest_blob_path);
    if(!file_exists(manifest_blob_path.buffer)) {
        SAH_TRACEZ_ERROR(ME, "Manifest blob '%s' does not exist? [%s:%s:%s]", manifest_blob_path.buffer, manifest->name, manifest->duid, manifest->version);
        goto exit;
    }

    manifest_schema = image_spec_schema_image_manifest_schema_parse_file(manifest_blob_path.buffer,
                                                                         NULL,
                                                                         &err);
    if(manifest_schema == NULL) {
        SAH_TRACEZ_ERROR(ME, "Couldnt parse manifest blob schema '%s': %s", manifest_blob_path.buffer, (err ? err : "?????"));
        goto exit;
    }

    amxc_llist_for_each(it, lblobs) {
        amxc_string_t* digest = amxc_llist_it_get_data(it, amxc_string_t, it);
        if(strcmp(digest->buffer, manifest_schema->config->digest) == 0) {
            // matches the digest of the blob config
            amxc_llist_it_clean(it, amxc_string_list_it_free);
        } else if(strcmp(digest->buffer, manifest->digest) == 0) {
            // matches the digest of the manifest blob itself
            amxc_llist_it_clean(it, amxc_string_list_it_free);
        } else if(manifest_schema->layers_len) {
            for(size_t it_layer = 0; it_layer < manifest_schema->layers_len; it_layer++) {
                if(strcmp(digest->buffer, manifest_schema->layers[it_layer]->digest) == 0) {
                    // matches one of the layer blob digest listed in the manifest itself
                    amxc_llist_it_clean(it, amxc_string_list_it_free);
                    break;
                }
            }
        }
    }
    free_image_spec_schema_image_manifest_schema(manifest_schema);
exit:
    amxc_string_clean(&manifest_blob_path);
    return;
}

typedef struct search_for_in_use_blobs_args {
    const char* storage_path;
    amxc_llist_t* list_of_blobs;
} search_for_in_use_blobs_args_t;

static void search_for_in_use_blobs_inner(UNUSED rlyeh_imagespec_index_t* index, rlyeh_imagespec_manifest_element_t* manifest, void* args) {
    search_for_in_use_blobs_args_t* parsed_args = (search_for_in_use_blobs_args_t*) args;
    take_used_blobs_from_list_using_manifest(parsed_args->list_of_blobs, parsed_args->storage_path, manifest);
}

static inline void filter_in_use_blobs(const char* storage_path, const char* image_path, amxc_llist_t* list_of_blobs) {
    rlyeh_images_list_t images_list;
    search_for_in_use_blobs_args_t args;
    args.storage_path = storage_path;
    args.list_of_blobs = list_of_blobs;

    rlyeh_images_list_init(&images_list);
    if(rlyeh_images_list_build(&images_list, image_path) > 0) {
        rlyeh_loop_over_images_list(&images_list, search_for_in_use_blobs_inner, &args);
    }
    rlyeh_images_list_clean(&images_list);
}

typedef struct remove_image_args {
    const char* images_path;
    size_t failed_counter;
} remove_image_args_t;


static void remove_image_inner(rlyeh_imagespec_index_t* index, rlyeh_imagespec_manifest_element_t* manifest, void* args) {
    int res;
    rlyeh_remove_data_t rm_data;
    remove_image_args_t* parsed_args = (remove_image_args_t*) args;

    rlyeh_remove_data_init(&rm_data);

    rm_data.duid = strdup(manifest->duid);
    rm_data.imageLocation = strdup(parsed_args->images_path);
    rm_data.version = strdup(manifest->version);
    rm_data.imageDiskLocation = strdup(index->image_disk_location);

    SAH_TRACEZ_INFO(ME, "Deleting orphan image spec (%s:%s) [%s]", \
                    rm_data.duid, rm_data.version, rm_data.imageDiskLocation);
    res = rlyeh_remove(&rm_data);
    if(res) {
        parsed_args->failed_counter++;
        SAH_TRACEZ_ERROR(ME, "Failed removing orphan image spec (%s:%s) [%s] in gc (%d)", \
                         rm_data.duid, \
                         rm_data.version, \
                         rm_data.imageDiskLocation, \
                         res);
    }
    rlyeh_remove_data_clean(&rm_data);
}

void rlyeh_remove_data_init(rlyeh_remove_data_t* data) {
    memset((void*) data, 0, sizeof(rlyeh_remove_data_t));
}

void rlyeh_remove_data_clean(rlyeh_remove_data_t* data) {
    if(!data) {
        return;
    }
    free(data->version);
    free(data->duid);
    free(data->imageLocation);
    free(data->imageDiskLocation);
    rlyeh_remove_data_init(data);
}

int rlyeh_remove(const rlyeh_remove_data_t* data) {
    int ret = -1;
    image_spec_schema_image_index_schema* index_schema = NULL;
    amxc_string_t index_filename;

    if((data->duid == NULL) || (data->duid[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, "Missing duid");
        return ret;
    }

    if((data->version == NULL) || (data->version[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, "Missing version");
        return ret;
    }

    if((data->imageDiskLocation == NULL) || (data->imageDiskLocation[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, "Missing imageDiskLocation");
        return ret;
    }

    amxc_string_init(&index_filename, 0);
    amxc_string_setf(&index_filename, "%s/index.json", data->imageDiskLocation);

    index_schema = rlyeh_parse_image_spec_schema(index_filename.buffer);
    if(!index_schema) {
        SAH_TRACEZ_ERROR(ME, "Cannot parse image spec schema '%s'", index_filename.buffer);
        goto exit;
    }

    if(index_schema->manifests_len > 1) { // There is an other version of the image
        SAH_TRACEZ_INFO(ME, "Updating index '%s' to remove manifest from the manifest elements", index_filename.buffer);
        ret = rlyeh_remove_manifest_from_image_spec_schema(index_schema, data->duid, data->version);
        if(ret) {
            SAH_TRACEZ_ERROR(ME, "Couldnt find image in the manifest elements");
        } else {
            int written = rlyeh_write_index_file_from_image_spec_schema(index_filename.buffer, index_schema);
            if((written > 0) && (errno != 0)) {
                ret = 0;
            }
        }
    } else {
        // Check if it really is the last image in the index as pre-caution
        size_t it;
        if(rlyeh_get_it_from_index(&it, index_schema, data->version, data->duid)) {
            SAH_TRACEZ_ERROR(ME, "Cannot find manifest element in the index [%s:%s], so obviously not removing the index", data->duid, data->version);
        } else {
            // Last version of the image to be deleted => delete the image directory
            SAH_TRACEZ_INFO(ME, "Removing index %s as there are no manifest elements", index_filename.buffer);
            ret = rlyeh_remove_file(index_filename.buffer);
            if(ret) {
                SAH_TRACEZ_ERROR(ME, "Cannot remove index %s (%d): %s", index_filename.buffer, errno, strerror(errno));
            } else {
                rlyeh_image_oci_layout_remove(data->imageDiskLocation);
                amxc_string_t blobsdir;
                amxc_string_init(&blobsdir, 0);
                amxc_string_setf(&blobsdir, "%s/blobs", data->imageDiskLocation);
                rlyeh_remove_blobs_dir(blobsdir.buffer);
                amxc_string_clean(&blobsdir);
                rlyeh_remove_image_dir(data->imageLocation, data->imageDiskLocation);
            }
        }
    }

    free_image_spec_schema_image_index_schema(index_schema);
exit:
    amxc_string_clean(&index_filename);
    return ret;
}

int rlyeh_remove_unlisted_blobs(const char* storagepath, const char* imagepath) {
    int ret = -1;
    amxc_llist_t lblobs;
    amxc_llist_init(&lblobs);

    if(storagepath == NULL) {
        goto exit;
    }

    ret = rlyeh_build_blobs_list(storagepath, &lblobs);
    if(ret > 0) {
        if(imagepath) {
            filter_in_use_blobs(storagepath, imagepath, &lblobs);
        }

        ret = 0;
        amxc_llist_for_each(it, &lblobs) {
            amxc_string_t* str = amxc_llist_it_get_data(it, amxc_string_t, it);
            if(rlyeh_remove_blob(str->buffer, storagepath)) {
                SAH_TRACEZ_ERROR(ME, "Failed Removing %s in %s: (%d) %s", str->buffer, storagepath, errno, strerror(errno));
                ret++;
            }
        }
        amxc_llist_clean(&lblobs, amxc_string_list_it_free);
    }

exit:
    return ret;
}

int rlyeh_remove_unlisted_images(const char* images_path,
                                 rlyeh_remove_data_t* imagestokeep,
                                 size_t imagestokeeplen) {
    int ret = -1;
    rlyeh_images_list_t images_list;

    if(STRING_EMPTY(images_path)) {
        goto exit;
    }

    rlyeh_images_list_init(&images_list);
    ret = rlyeh_images_list_build(&images_list, images_path);
    if(ret > 0) {
        if(imagestokeeplen) {
            rlyeh_remove_images_from_images_list(&images_list, imagestokeep, imagestokeeplen);
        }

        // All images in the images_list will be removed
        // eg --> unlisted images
        {
            remove_image_args_t args;
            args.images_path = images_path;
            args.failed_counter = 0;
            rlyeh_loop_over_images_list(&images_list, remove_image_inner, &args);
            ret = (int) args.failed_counter;
        }
    }
    rlyeh_images_list_clean(&images_list);
exit:
    return ret;
}

typedef struct remove_image_from_images_list_args {
    const rlyeh_remove_data_t* images_to_keep;
    size_t images_to_keep_len;
    bool* matched;
} remove_image_from_images_list_args_t;

static void remove_image_from_images_list_inner(UNUSED rlyeh_imagespec_index_t* index, rlyeh_imagespec_manifest_element_t* manifest, void* args) {
    remove_image_from_images_list_args_t* parsed_args = (remove_image_from_images_list_args_t*) args;

    for(size_t it = 0; it < parsed_args->images_to_keep_len; it++) {
        if(parsed_args->matched[it] == false) {
            const rlyeh_remove_data_t* ptr = parsed_args->images_to_keep + it;
            if(manifest->duid && (strcmp(ptr->duid, manifest->duid) == 0)) {
                if(manifest->version && (strcmp(ptr->version, manifest->version) == 0)) {
                    if(strcmp(ptr->imageDiskLocation, index->image_disk_location) == 0) {
                        rlyeh_remove_manifest_element_from_index(manifest);
                        parsed_args->matched[it] = true;
                        break;
                    }
                }
            }
        }
    }
}

void rlyeh_remove_images_from_images_list(rlyeh_images_list_t* images_list, rlyeh_remove_data_t* imagestokeep, size_t imagestokeeplen) {
    remove_image_from_images_list_args_t args;
    args.images_to_keep = imagestokeep;
    args.images_to_keep_len = imagestokeeplen;
    args.matched = (bool*) calloc(imagestokeeplen, sizeof(bool));
    rlyeh_loop_over_images_list(images_list, remove_image_from_images_list_inner, &args);
    free(args.matched);
}

void rlyeh_remove_data_arr_clean(rlyeh_remove_data_t* images, size_t len) {
    for(size_t i = 0; i < len; i++) {
        rlyeh_remove_data_clean(images + i);
    }
}

rlyeh_remove_data_t* rlyeh_remove_data_arr_allocate(size_t len) {
    return calloc(len, sizeof(rlyeh_remove_data_t));
}
