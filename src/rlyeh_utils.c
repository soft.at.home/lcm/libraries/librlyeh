/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <unistd.h>
#include <openssl/evp.h>
#include <dirent.h>
#include <ftw.h>

#include <rlyeh/rlyeh_utils.h>
#include <rlyeh_priv.h>
#include <rlyeh_defines_priv.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxj/amxj_variant.h>

#include <debug/sahtrace.h>

#include <lcm/lcm_assert.h>

#define ME "rlyeh_utils"

static int nftw_remove_files(const char* pathname, UNUSED const struct stat* sbuf, UNUSED int type, UNUSED struct FTW* ftwb) {
    if(remove(pathname) < 0) {
        return -1;
    }
    return 0;
}

int rlyeh_remove_dir_unsafe(const char* path_to_dir) {
    int errno_copy = 0;
    int ret = -1;

    if(path_to_dir == NULL) {
        goto exit;
    }

    ret = nftw(path_to_dir, nftw_remove_files, 10, FTW_DEPTH | FTW_MOUNT | FTW_PHYS);
    if(ret < 0) {
        errno_copy = errno;
        SAH_TRACEZ_ERROR(ME, "Couldnt ntfw %s: (%d) %s", path_to_dir, errno_copy, strerror(errno_copy));
    }

    errno = errno_copy;
exit:
    return ret;
}

void print_file(const char* zone, const char* path) {
    char buf[1024];
    int buflen;

    if(path == NULL) {
        SAH_TRACEZ_ERROR(ME, "Path is NULL");
        return;
    }

    int fd = open(path, O_RDONLY);
    if(fd < 0) {
        SAH_TRACEZ_ERROR(ME, "Cannot open '%s'", path);
        return;
    }

    SAH_TRACEZ_INFO(zone, "Printing '%s'\n--- ---\n", path);
    while((buflen = read(fd, buf, 1024)) > 0) {
        buf[buflen] = '\0';
        SAH_TRACEZ_ERROR(ME, "%s", buf);
    }
    close(fd);
}

int rlyeh_file_is_symlink(const char* filename) {
    int help;
    struct stat buf;

    if(filename == NULL) {
        return -2;
    }

    help = lstat(filename, &buf);
    if(help == 0) {
        if(S_ISLNK(buf.st_mode)) {
            return 1;
        }
        return 0;
    }
    return -1;
}

bool file_exists(const char* filename) {
    bool res = false;
    when_null(filename, exit);
    if(access(filename, F_OK) == 0) {
        res = true;
    }
exit:
    return res;
}

bool dir_exists(const char* dirname) {
    if(rlyeh_is_dir(dirname) == 1) {
        return true;
    }
    return false;
}

ssize_t rlyeh_write_file(const char* file_path, const char* data) {
    int fd = open(file_path, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    ssize_t written = 0;
    ssize_t len_data = strlen(data);
    int n;
    int errno_copy = 0;

    do {
        if((n = write(fd, (data + written), (len_data - written))) < 0) {
            if((errno == EINTR) || (errno == EAGAIN)) {
                continue;
            } else {
                errno_copy = errno;
                written = -1;
                SAH_TRACEZ_ERROR(ME, "Cannot write file %s: (%d) %s", file_path, errno_copy, strerror(errno_copy));
                break;
            }
        }
        written = written + n;
    } while(written < len_data);

    close(fd);
    if(written < 1) {
        remove(file_path);
    }
    errno = errno_copy;
    return written;
}

// To know if an image already exists, we need to know if there is a directory with the image name
// AND if the index.json contains the version annotation and duid.
bool rlyeh_image_already_exists(const rlyeh_image_parameters_t* image, const char* disk_location) {
    bool ret = false;
    when_null(image, exit);
    if(!image->duid || !image->version) {
        SAH_TRACEZ_INFO(ME, "No image duid or version");
        return ret;
    }

    amxc_string_t index_filename;
    amxc_string_init(&index_filename, 0);
    amxc_string_setf(&index_filename, "%s/index.json", disk_location);
    image_spec_schema_image_index_schema* index_schema = rlyeh_parse_image_spec_schema(index_filename.buffer);

    if(index_schema) {
        size_t it_manifests;
        if(rlyeh_get_it_from_index(&it_manifests, index_schema, image->version, image->duid) == 0) {
            ret = true;
        }
        free_image_spec_schema_image_index_schema(index_schema);
    }

    amxc_string_clean(&index_filename);
exit:
    return ret;
}

int rlyeh_image_parameters_init(rlyeh_image_parameters_t* data) {
    if(data) {
        memset((void*) data, 0, sizeof(rlyeh_image_parameters_t));
        data->auth_type = NO_AUTHENTICATION;
        return 0;
    }
    return -1;
}

int rlyeh_image_parameters_clean(rlyeh_image_parameters_t* data) {
    if(data) {
        free_and_set_to_null(data->transport);
        free_and_set_to_null(data->duid);
        free_and_set_to_null(data->image_name);
        free_and_set_to_null(data->image_dir);
        free_and_set_to_null(data->version);
        free_and_set_to_null(data->signature_url_overload);
        free_and_set_to_null(data->server);
        free_and_set_to_null(data->server_url_append);
        free_and_set_to_null(data->username);
        free_and_set_to_null(data->password);
        free_and_set_to_null(data->token);
        return 0;
    }
    return -1;
}

static bool rlyeh_check_if_server(const char* server_or_dir) {
    const char* retptr = server_or_dir;
    while(retptr[0] != '\0') {
        if(retptr[0] == '.') {
            return true;
        }
        retptr++;
    }
    return false;
}

// Parse path : transport://server/image_name:version and transport://image_dir:version
int rlyeh_parse_uri(const char* uri, rlyeh_image_parameters_t* image_params) {
    int ret = -2;
    char* saveptr = NULL;
    char* uricpy;
    char* retptr;

    when_null(uri, exit);
    when_null(image_params, exit);

    uricpy = strdup(uri);
    saveptr = uricpy;
    retptr = uricpy;

    ret++;
    while(saveptr[0] != '\0') {
        if(saveptr[0] == ':') {
            (saveptr++)[0] = '\0';
            if((saveptr[0] == '/') && (saveptr[1] == '/')) {
                (saveptr++)[0] = '\0';
                (saveptr++)[0] = '\0';
            }
            break;
        }
        saveptr++;
    }

    if(saveptr[0] == '\0') {
        goto exitclean;
    }

    ret++;
    image_params->transport = strdup(retptr);

    retptr = saveptr;
    while(saveptr[0] != '\0') {
        if(saveptr[0] == ':') {
            (saveptr++)[0] = '\0';
            break;
        }
        saveptr++;
    }

    // can be "server/group/image_name" or "/image_dir1/image_dir2/blabla"
    if(retptr[0] == '/') {
        // Easy for Absolute paths
        image_params->image_dir = strdup(retptr);
    } else {
        char* saveptr2 = retptr;
        while(saveptr2[0] != '\0') {
            if(saveptr2[0] == '/') {
                (saveptr2++)[0] = '\0';
                break;
            }
            saveptr2++;
        }

        if(saveptr2[0] == '\0') {
            // it was only an image_name
            image_params->image_name = strdup(retptr);
        } else if(rlyeh_check_if_server(retptr)) {
            image_params->server = strdup(retptr);
            image_params->image_name = strdup(saveptr2);
        } else {
            // Restore string again to have a "full" relative path
            // btw it could still be a group/image_name here
            // luckily the union saves us here
            saveptr2[-1] = '/';
            image_params->image_dir = strdup(retptr);
        }
    }

    if(saveptr[0] == '\0') {
        image_params->version = strdup("latest");
    } else {
        image_params->version = strdup(saveptr);
    }

exitclean:
    free(uricpy);
exit:
    return ret;
}

amxc_var_t* read_config(const char* file) {
    amxc_var_t* ret = NULL;
    int read_length = 0;
    int fd = 0;
    variant_json_t* reader = NULL;

    fd = open(file, O_RDONLY);
    if(fd < 0) {
        SAH_TRACEZ_INFO(ME, "Could not open file [%s] (%s)", file, strerror(errno));
        goto exit;
    }

    amxj_reader_new(&reader);
    when_null(reader, exitclean);

    do {
        read_length = amxj_read(reader, fd);
    } while(read_length > 0);

    ret = amxj_reader_result(reader);

    amxj_reader_delete(&reader);
exitclean:
    close(fd);
exit:
    return ret;
}

int rlyeh_get_server_from_file(rlyeh_image_parameters_t* param) {
    int ret = -1;

    amxc_var_t* config_data = NULL;
    when_null(param, exit);
    config_data = read_config("/etc/amx/librlyeh/registry-auth.json");

    if(config_data) {
        amxc_var_t* credentials = GET_ARG(config_data, "credentials");
        if(credentials) {
            amxc_var_t* var = amxc_var_take_index(credentials, 0);
            if(var) {
                amxc_var_t* var_server = amxc_var_get_path(var, "server", AMXC_VAR_FLAG_DEFAULT);
                if(var_server) {
                    const char* str_server = amxc_var_constcast(cstring_t, var_server);
                    if(str_server) {
                        free(param->server);
                        param->server = strdup(str_server);
                        ret = 0;
                    }
                    amxc_var_delete(&var_server);
                }
                amxc_var_delete(&var);
            }
            amxc_var_delete(&credentials);
        }
        amxc_var_delete(&config_data);
    }

exit:
    return ret;
}

int rlyeh_get_creds_from_file(rlyeh_image_parameters_t* data, const char* server) {
    int res = -1;
    amxc_var_t* config_data = NULL;
    amxc_var_t* credentials = NULL;
    when_null(server, exit);
    config_data = read_config("/etc/amx/librlyeh/registry-auth.json");
    when_null(config_data, exit);
    credentials = GET_ARG(config_data, "credentials");
    if(credentials) {
        amxc_var_for_each(var, credentials) {
            amxc_var_t* var_name = amxc_var_get_path(var, "server", AMXC_VAR_FLAG_DEFAULT);
            if(var_name) {
                const char* str_name = amxc_var_constcast(cstring_t, var_name);
                if(strcmp(server, str_name) == 0) {
                    amxc_var_t* var_username = amxc_var_get_path(var, "username", AMXC_VAR_FLAG_DEFAULT);
                    amxc_var_t* var_password = amxc_var_get_path(var, "password", AMXC_VAR_FLAG_DEFAULT);
                    if(var_username && var_password) {
                        const char* str_username = amxc_var_constcast(cstring_t, var_username);
                        const char* str_password = amxc_var_constcast(cstring_t, var_password);

                        free(data->username);
                        data->username = strdup(str_username);

                        free(data->password);
                        data->password = strdup(str_password);

                        amxc_var_delete(&var_username);
                        amxc_var_delete(&var_password);

                        res = 0;
                    }
                }
                amxc_var_delete(&var_name);
            }
        }
    }

    amxc_var_delete(&credentials);
    amxc_var_delete(&config_data);
exit:
    return res;
}

int rlyeh_get_it_from_index(size_t* it,
                            const image_spec_schema_image_index_schema* index_schema,
                            const char* image_version,
                            const char* image_duid) {
    int ret = -1;
    bool bversion = false;
    bool bduid = false;
    size_t manifests_len = index_schema->manifests_len;

    for(*it = 0; (*it) < manifests_len; (*it)++) {
        if(index_schema->manifests[*it]->annotations) {
            size_t it_annotations = 0;
            size_t annotations_len = index_schema->manifests[*it]->annotations->len;
            for(; it_annotations < annotations_len; it_annotations++) {
                const char* annotation_key = index_schema->manifests[*it]->annotations->keys[it_annotations];
                const char* annotation_value = index_schema->manifests[*it]->annotations->values[it_annotations];
                if(strcmp(RLYEH_ANNOTATION_LCM_DUID, annotation_key) == 0) {
                    if(strcmp(image_duid, annotation_value) == 0) {
                        bduid = true;
                        if(bversion) {
                            break;
                        }
                    } else {
                        break;
                    }
                } else if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_REFNAME, annotation_key) == 0) {
                    if(strcmp(image_version, annotation_value) == 0) {
                        bversion = true;
                        if(bduid) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        if(bversion && bduid) {
            ret = 0;
            break;
        }
        bversion = false;
        bduid = false;
    }

    return ret;
}

bool build_manifest_filename_from_index(const image_spec_schema_image_index_schema* index_schema,
                                        const char* image_version,
                                        const char* image_duid,
                                        const char* storage_location,
                                        amxc_string_t* manifest_filename) {
    size_t it_manifest = 0;

    if(rlyeh_get_it_from_index(&it_manifest, index_schema, image_version, image_duid) == 0) {
        build_fname_from_digest(storage_location, index_schema->manifests[it_manifest]->digest, manifest_filename);
        return false;
    }

    return true;
}

bool check_manifest_validity(const image_spec_schema_image_manifest_schema* manifest, UNUSED parser_error err) {
    if(manifest == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not parse Image Manifest file, Err: %s", err);
        return true;
    }

    if(manifest->schema_version_present && (manifest->schema_version != 2)) {
        SAH_TRACEZ_ERROR(ME, "The manifest has an invalid schema version. (!=2)");
        SAH_TRACEZ_ERROR(ME, "Schema version: %d\n", manifest->schema_version);
        return true;
    }

    if(manifest->config == NULL) {
        SAH_TRACEZ_ERROR(ME, "image config is NULL");
        return true;
    }

    // if(strcmp("application/vnd.oci.image.config.v1+json",
    //           manifest->config->media_type) != 0) {
    //     SAH_TRACEZ_ERROR(ME, "Unexpected config type: [%s] Expected [application/vnd.oci.image.config.v1+json]", manifest->config->media_type);
    //     return true;
    // }

    return false;
}

ssize_t rlyeh_copy_file(const char* src, const char* dest) {
    ssize_t ret = -1;
    int fromfd, tofd;
    int errno_copy = 0;

    if((fromfd = open(src, O_RDONLY)) < 0) {
        errno_copy = errno;
        SAH_TRACEZ_ERROR(ME, "Can't open src '%s': (%d) %s", src, errno, strerror(errno));
        goto exitnoclose;
    }

    if((tofd = open(dest, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) < 0) {
        errno_copy = errno;
        SAH_TRACEZ_ERROR(ME, "Can't open dest '%s': (%d) %s", dest, errno, strerror(errno));
        close(fromfd);
        goto exitclosefromfd;
    }

#if defined(__linux__)
    struct stat st;
    st.st_size = 0;
    off_t off = 0;

    if(stat(src, &st) < 0) {
        errno_copy = errno;
        SAH_TRACEZ_ERROR(ME, "Can't stat '%s': (%d) %s", src, errno, strerror(errno));
        goto exit;
    }

    while(off < st.st_size) {
        ret = sendfile(tofd, fromfd, &off, (st.st_size - off));
        if(ret == -1) {
            if((errno == EINTR) || (errno == EAGAIN)) {
                continue;
            } else {
                SAH_TRACEZ_WARNING(ME, "Warning: sendfile(3EXT) returned %zd: (%d) %s)", ret, errno, strerror(errno));
                break;
            }
        }
    }

    if(off != 0) {
        if(off == st.st_size) {
            ret = st.st_size;
            goto exit;
        }
        if(lseek(tofd, 0, SEEK_SET) < 0) {
            errno_copy = errno;
            SAH_TRACEZ_ERROR(ME, "Cannot reset offset in dest: (%d) %s)", errno, strerror(errno));
            goto exit;
        }
        if(lseek(fromfd, 0, SEEK_SET) < 0) {
            errno_copy = errno;
            SAH_TRACEZ_ERROR(ME, "Cannot reset offset in src: (%d) %s)", errno, strerror(errno));
            goto exit;
        }
    }
#endif

    {
        char buf[4096];
        ssize_t nread;
        ret = 0;
        while((nread = read(fromfd, buf, sizeof(buf))) > 0) {
            char* out_ptr = buf;
            do {
                ssize_t nwritten = write(tofd, out_ptr, nread);
                if(nwritten > 0) {
                    nread -= nwritten;
                    out_ptr += nwritten;
                    ret += nwritten;
                } else if((errno == EINTR) || (errno == EAGAIN)) {
                    continue;
                } else {
                    errno_copy = errno;
                    ret = -1;
                    goto exit;
                }
            } while(nread > 0);
        }
    }

exit:
    close(tofd);
    if(ret < 1) {
        remove(dest);
    }
exitclosefromfd:
    close(fromfd);
exitnoclose:
    errno = errno_copy;
    return ret;
}

int rlyeh_move_file(const char* src, const char* dest) {
    int ret = 0;
    int errno_copy = 0;

    if(rename(src, dest)) {
        errno_copy = errno;
        if(errno == EXDEV) {
            ret = rlyeh_copy_file(src, dest);
            errno_copy = errno;
            if(ret > 0) {
                remove(src);
                ret = 0;
            } else if(ret == 0) {
                ret = -1;
                remove(dest);
            }
        } else {
            ret = -1;
        }
    }

    errno = errno_copy;
    return ret;
}

int64_t rlyeh_hash_blob(const char* filename, const char* hash_algo, char** hash_blob) {
    int64_t ret = -1;
    size_t total_bytes = 0;
    int64_t read_bytes = 0;
    EVP_MD_CTX* mdctx = NULL;
    unsigned char buffer[4096] = {};
    unsigned char md_value[EVP_MAX_MD_SIZE];
    const EVP_MD* md;
    char* hash_blob_buff = NULL;
    int evp_md_size = 0;
    size_t hash_blob_size = 0;

    if(strcmp(hash_algo, "sha256") == 0) {
        hash_blob_size = 65;
        md = EVP_sha256();
    } else if(strcmp(hash_algo, "sha512") == 0) {
        hash_blob_size = 129;
        md = EVP_sha512();
    } else {
        SAH_TRACEZ_ERROR(ME, "Dont know how to generate %s hash", hash_algo);
        return ret;
    }

    evp_md_size = EVP_MD_size(md);
    if(evp_md_size < 1) {
        SAH_TRACEZ_ERROR(ME, "Cannot get EVP MD size of %s", hash_algo);
        return ret;
    }

    FILE* file = fopen(filename, "rb");
    if(!file) {
        goto exitclean;
    }

    if(!(mdctx = EVP_MD_CTX_new())) {
        SAH_TRACEZ_ERROR(ME, "Failed to create EVP_MD_CTX");
        goto exitcleanfd;
    }

    if(!EVP_DigestInit_ex(mdctx, md, NULL)) {
        goto exitcleanctx;
    }

    while((read_bytes = fread(&buffer, 1, sizeof(buffer), file))) {
        if(!EVP_DigestUpdate(mdctx, &buffer, read_bytes)) {
            goto exitcleanctx;
        }
        total_bytes += (int64_t) read_bytes;
    }

    if(!EVP_DigestFinal_ex(mdctx, md_value, NULL)) {
        SAH_TRACEZ_ERROR(ME, "Failed to finalize digest");
        goto exitcleanctx;
    }

    hash_blob_buff = malloc(hash_blob_size);
    hash_blob_buff[hash_blob_size - 1] = '\0';

    for(int i = 0; i < evp_md_size; i++) {
        sprintf((hash_blob_buff + (i * 2)), "%02x", md_value[i]);
    }

    ret = total_bytes;
    *hash_blob = hash_blob_buff;
exitcleanctx:
    EVP_MD_CTX_free(mdctx);
exitcleanfd:
    fclose(file);
exitclean:
    return ret;
}

int rlyeh_generate_uri(rlyeh_image_parameters_t* uri_elements, amxc_string_t* full_uri, const char* images_location) {
    int res = -1;
    when_null(uri_elements, exit);
    when_null(full_uri, exit);

    if(uri_elements->server) {
        amxc_string_setf(full_uri, "%s://%s/%s:%s", uri_elements->transport, uri_elements->server, uri_elements->image_name, uri_elements->version);
    } else {
        if(images_location) {
            amxc_string_setf(full_uri, "%s://%s/%s:%s", uri_elements->transport, images_location, uri_elements->image_name, uri_elements->version);
        } else {
            amxc_string_setf(full_uri, "%s://%s:%s", uri_elements->transport, uri_elements->image_dir, uri_elements->version);
        }
    }

    res = 0;
exit:
    return res;
}

int build_fname_from_digest(const char* storage_location, const char* digest, amxc_string_t* fname) {
    int res = -1;
    char* cpy_digest = NULL;
    char* pos = NULL;
    when_null(storage_location, exit);
    when_null(digest, exit);
    when_null(fname, exit);

    cpy_digest = strdup(digest);
    if((pos = strchr(cpy_digest, ':')) != NULL) {
        *pos = '/';
    }
    amxc_string_setf(fname, "%s/%s", storage_location, cpy_digest);
    res = 0;
exit:
    free(cpy_digest);
    return res;

}

int set_err_msg(char* dest, const char* msg, ...) {
    int res = 0;
    if(!dest) {
        return 0;
    }
    va_list myargs;
    va_start(myargs, msg);
    res = vsnprintf(dest, RLYEH_ERR_MSG_LEN - 1, msg, myargs);
    va_end(myargs);
    return res;
}

image_spec_schema_image_index_schema* rlyeh_parse_image_spec_schema(const char* image_name) {
    image_spec_schema_image_index_schema* image_spec_schema = NULL;
    parser_error err = NULL;
    if(!image_name) {
        SAH_TRACEZ_ERROR(ME, "Image name is null");
        goto exit;
    }
    if(!file_exists(image_name)) {
        SAH_TRACEZ_WARNING(ME, "File does not exist: %s", image_name);
        goto exit;
    }

    image_spec_schema = image_spec_schema_image_index_schema_parse_file(image_name, 0, &err);
    if(!image_spec_schema) {
        SAH_TRACEZ_ERROR(ME, "[%s] Index schema is invalid: '%s'", image_name, err ? err : "???");
        SAH_TRACEZ_ERROR(ME, "Removing the invalid index schema");
        rlyeh_remove_file(image_name);
        free(err);
    }
exit:
    return image_spec_schema;
}

char* digest_to_path(const char* digest) {
    // Modify sha256:ad23e3c2fa54c2e1... TO sha256/ad23e3c2fa54c2e1...
    char* pos = NULL;
    char* blob_file;

    if((digest == NULL) || (digest[0] == '\0')) {
        return NULL;
    }

    blob_file = (char*) malloc(strlen(digest) + 1);
    strcpy(blob_file, digest);
    if((pos = strchr(blob_file, ':')) != NULL) {
        *pos = '/';
    }
    // remember to free
    return blob_file;
}

void rlyeh_signature_data_init(rlyeh_signature_data_t* data) {
    data->uri = NULL;
    data->transport = NULL;
    data->server = NULL;
    data->image_name = NULL;
    data->version = NULL;
    data->manifest_hash = NULL;
    data->signature_url_overload = NULL;
}

void rlyeh_signature_data_clean(rlyeh_signature_data_t* data) {
    if(!data) {
        return;
    }
    free(data->uri);
    free(data->transport);
    free(data->server);
    free(data->image_name);
    free(data->version);
    free(data->manifest_hash);
    free(data->signature_url_overload);
}

void rlyeh_loop_through_index_files(const char* image_location, index_loop_callback_t fn, void* args) {
    struct dirent* de;
    amxc_string_t new_image_location;

    DIR* dr = opendir(image_location);
    if(dr != NULL) {
        amxc_string_init(&new_image_location, 0);

        while((de = readdir(dr)) != NULL) {
            bool isdir = false;
            bool isfile = false;
            if(CHECK_DIR_DOT_DOT(de->d_name)) {
                continue;
            }

            amxc_string_setf(&new_image_location, "%s/%s", image_location, de->d_name);

            switch(de->d_type) {
            case DT_LNK:
            case DT_UNKNOWN:
                if(rlyeh_is_dir(new_image_location.buffer) == 1) {
                    isdir = true;
                } else if(rlyeh_is_file(new_image_location.buffer) == 1) {
                    isfile = true;
                } else {
                    continue;
                }
                break;
            case DT_DIR:
                isdir = true;
                break;
            case DT_REG:
                isfile = true;
                break;
            default:
                continue;
            }

            if(isdir) {
                rlyeh_loop_through_index_files(new_image_location.buffer, fn, args);
            } else if(isfile) {
                if(strcmp(de->d_name, "index.json") == 0) {
                    fn(image_location, new_image_location.buffer, args);
                }
            }
        }

        amxc_string_clean(&new_image_location);
        closedir(dr);
    }
}

bool rlyeh_is_empty_dir(const char* dirname) {
    int n = 0;
    struct dirent* de;

    if(dirname == NULL) {
        return true;
    }

    DIR* dir = opendir(dirname);
    if(dir) {
        while((de = readdir(dir)) != NULL) {
            if(CHECK_DIR_DOT_DOT(de->d_name)) {
                continue;
            }
            n++;
            break;
        }
        closedir(dir);
    }
    return (n == 0);
}

void rlyeh_remove_all_files_in_dir(const char* path_to_dir) {
    struct dirent* de;
    DIR* dr;

    dr = opendir(path_to_dir);
    if(dr != NULL) {
        amxc_string_t filepath;
        amxc_string_init(&filepath, 0);
        while((de = readdir(dr)) != NULL) {
            if(CHECK_DIR_DOT_DOT(de->d_name)) {
                continue;
            }

            amxc_string_setf(&filepath, "%s/%s", path_to_dir, de->d_name);

            switch(de->d_type) {
            case DT_LNK:
            case DT_UNKNOWN:
                if(rlyeh_is_file(filepath.buffer) != 1) {
                    continue;
                }
                break;
            case DT_REG:
                break;
            default:
                continue;
            }

            rlyeh_remove_file(filepath.buffer);
        }
        amxc_string_clean(&filepath);
        closedir(dr);
    }
}

int rlyeh_remove_blobs_dir(const char* blobs_dir) {
    int ret = -1;
    struct dirent* de;
    DIR* dr;

    dr = opendir(blobs_dir);
    if(dr != NULL) {
        amxc_string_t hashdirpath;
        amxc_string_init(&hashdirpath, 0);
        while((de = readdir(dr)) != NULL) {
            if(CHECK_DIR_DOT_DOT(de->d_name)) {
                continue;
            }

            amxc_string_setf(&hashdirpath, "%s/%s", blobs_dir, de->d_name);

            switch(de->d_type) {
            case DT_LNK:
            case DT_UNKNOWN:
                if(rlyeh_is_dir(hashdirpath.buffer) != 1) {
                    continue;
                }
                break;
            case DT_DIR:
                break;
            default:
                continue;
            }

            rlyeh_remove_all_files_in_dir(hashdirpath.buffer);
            ret = rmdir(hashdirpath.buffer);
            if(ret) {
                SAH_TRACEZ_ERROR(ME, "Cannot remove Blob Hash Dir %s (%d): %s", hashdirpath.buffer, errno, strerror(errno));
            }
        }
        amxc_string_clean(&hashdirpath);
        closedir(dr);

        ret = rmdir(blobs_dir);
        if(ret) {
            SAH_TRACEZ_ERROR(ME, "Cannot remove Blobs dir %s (%d): %s", blobs_dir, errno, strerror(errno));
        }
    }

    return ret;
}

int rlyeh_build_blobs_list(const char* storage_path, amxc_llist_t* lblobs) {
    int ret = -1;
    struct dirent* de;
    DIR* dr;

    dr = opendir(storage_path);
    if(dr != NULL) {
        ret = 0;
        amxc_string_t blobsdir;
        amxc_string_t blob_path;
        amxc_string_t blob_digest;

        amxc_string_init(&blobsdir, 0);
        amxc_string_init(&blob_digest, (( 32 * 2) + 1 + 1));
        amxc_string_init(&blob_path, 0);

        while((de = readdir(dr)) != NULL) {
            if(CHECK_DIR_DOT_DOT(de->d_name)) {
                continue;
            }

            amxc_string_setf(&blobsdir, "%s/%s", storage_path, de->d_name);

            switch(de->d_type) {
            case DT_LNK:
            case DT_UNKNOWN:
                if(rlyeh_is_dir(blobsdir.buffer) != 1) {
                    continue;
                }
                break;
            case DT_DIR:
                break;
            default:
                continue;
            }

            {
                struct dirent* sub_de;
                DIR* sub_dr = opendir(blobsdir.buffer);
                if(sub_dr != NULL) {
                    while((sub_de = readdir(sub_dr)) != NULL) {
                        if(CHECK_DIR_DOT_DOT(sub_de->d_name)) {
                            continue;
                        }

                        switch(sub_de->d_type) {
                        case DT_LNK:
                        case DT_UNKNOWN:
                            amxc_string_setf(&blob_path, "%s/%s", blobsdir.buffer, sub_de->d_name);
                            if(rlyeh_is_file(blob_path.buffer) != 1) {
                                continue;
                            }
                            break;
                        case DT_REG:
                            break;
                        default:
                            continue;
                        }

                        amxc_string_setf(&blob_digest, "%s:%s", de->d_name, sub_de->d_name);
                        amxc_llist_add_string(lblobs, blob_digest.buffer);
                        ret++;
                    }
                    closedir(sub_dr);
                }
            }
        }
        amxc_string_clean(&blobsdir);
        amxc_string_clean(&blob_path);
        amxc_string_clean(&blob_digest);
        closedir(dr);
    }

    return ret;
}

int rlyeh_remove_blob(const char* digest, const char* storage_location) {
    int ret = -1;
    int errno_copy = 0;
    amxc_string_t fname;
    amxc_string_init(&fname, 0);

    ret = build_fname_from_digest(storage_location, digest, &fname);
    if(ret) {
        SAH_TRACEZ_ERROR(ME, "Couldnt build fname from digest");
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Removing blob %s", fname.buffer);
    ret = rlyeh_remove_file(fname.buffer);
    if(ret) {
        errno_copy = errno;
    }

exit:
    amxc_string_clean(&fname);
    errno = errno_copy;
    return ret;
}

int rlyeh_remove_file(const char* file_path) {
    int errno_copy = 0;
    int ret = -3;
    if(file_exists(file_path)) {
        ret = unlink(file_path);
        errno_copy = errno;
        if(ret) {
            SAH_TRACEZ_ERROR(ME, "Couldn't unlink %s (%d): %s", file_path, errno_copy, strerror(errno_copy));
        }
    } else {
        SAH_TRACEZ_WARNING(ME, "%s... doesn't exist", file_path);
        ret = -2;
    }
    errno = errno_copy;
    return ret;
}

int rlyeh_remove_image_dir(const char* image_location, const char* image_dir) {
    int errno_copy = 0;
    int ret = -1;

    // normalize image_location
    ssize_t index_last_char = strlen(image_location) - 1;
    if(index_last_char < 1) {
        return ret;
    }
    char* imageloccpy = strdup(image_location);
    if(imageloccpy[index_last_char] == '/') {
        imageloccpy[index_last_char] = '\0';
    }

    index_last_char = strlen(image_dir) - 1;
    char* strcpy = strdup(image_dir);
    if(strcpy[index_last_char] == '/') {
        strcpy[index_last_char] = '\0';
    }

    while(strcmp(imageloccpy, strcpy) != 0) {
        SAH_TRACEZ_INFO(ME, "Trying to remove image dir %s", strcpy);
        ret = rmdir(strcpy);
        if(ret) {
            errno_copy = errno;
            SAH_TRACEZ_ERROR(ME, "Cannot rmdir %s: (%d) %s",
                             strcpy, \
                             errno_copy, \
                             strerror(errno_copy));
            break;
        }

        for(size_t i = strlen(strcpy); i; --i) {
            if(strcpy[i] == '/') {
                strcpy[i] = '\0';
                break;
            }
        }
    }

    free(imageloccpy);
    free(strcpy);
    errno = errno_copy;
    return ret;
}

int rlyeh_write_index_file_from_image_spec_schema(const char* index_filename, image_spec_schema_image_index_schema* index_schema) {
    int ret = -1;
    char* image_index_json = NULL;
    parser_error err = NULL;

    image_index_json = image_spec_schema_image_index_schema_generate_json(index_schema, 0, &err);
    if(!image_index_json) {
        SAH_TRACEZ_ERROR(ME, "Couldnt generate the image index json: '%s'", err);
        free(err);
        goto exit;
    }

    ret = rlyeh_write_file(index_filename, image_index_json);
    free(image_index_json);
exit:
    return ret;
}

int rlyeh_remove_manifest_from_image_spec_schema(image_spec_schema_image_index_schema* index_schema, const char* duid, const char* version) {
    int ret = -1;
    // Update manifests and manifest_len in index.json
    size_t manifests_len = index_schema->manifests_len;
    // Otherwise json serializes an empty structure too much
    size_t updated_manifests_len = manifests_len - 1;
    size_t it;
    size_t it_ptr = 0;

    if(rlyeh_get_it_from_index(&it, index_schema, version, duid) != 0) {
        SAH_TRACEZ_ERROR(ME, "Cannot find manifest element in the index, so obviously not removing it");
        goto exit;
    }

    image_spec_schema_image_index_schema_manifests_element** ptr_manifests =
        (image_spec_schema_image_index_schema_manifests_element**) calloc(updated_manifests_len, sizeof(image_spec_schema_image_index_schema_manifests_element*));
    for(size_t i = 0; i < manifests_len; i++) {
        if(i == it) {
            free_image_spec_schema_image_index_schema_manifests_element(index_schema->manifests[i]);
        } else {
            ptr_manifests[it_ptr++] = index_schema->manifests[i];
        }
    }
    index_schema->manifests_len = updated_manifests_len;
    free(index_schema->manifests);
    index_schema->manifests = ptr_manifests;

    ret = 0;
exit:
    return ret;
}

void rlyeh_update_annotation_in_manifest_element(image_spec_schema_image_index_schema_manifests_element* manifest,
                                                 const char* annotation_key,
                                                 const char* annotation_value) {
    bool found = false;
    json_map_string_string* annotations_json = (json_map_string_string*) calloc(1, sizeof(json_map_string_string));

    if(manifest->annotations) {
        for(size_t it_annotation = 0; it_annotation < manifest->annotations->len; it_annotation++) {
            const char* key = manifest->annotations->keys[it_annotation];
            const char* value = manifest->annotations->values[it_annotation];
            if(strcmp(annotation_key, key) == 0) {
                append_json_map_string_string(annotations_json, annotation_key, annotation_value);
                found = true;
            } else {
                append_json_map_string_string(annotations_json, key, value);
            }
        }
    }

    if(found == false) {
        append_json_map_string_string(annotations_json, annotation_key, annotation_value);
    }

    if(manifest->annotations) {
        free_json_map_string_string(manifest->annotations);
    }
    manifest->annotations = annotations_json;

    return;
}

int rlyeh_is_file(const char* filename) {
    struct stat sb;
    int ret = -1;

    when_null(filename, exit);

    if(stat(filename, &sb) == 0) {
        if(sb.st_mode & S_IFREG) {
            ret = 1;
        } else {
            ret = 0;
        }
    }

exit:
    return ret;
}


int rlyeh_is_dir(const char* dirname) {
    struct stat sb;
    int ret = -1;

    when_null(dirname, exit);

    if(stat(dirname, &sb) == 0) {
        if(sb.st_mode & S_IFDIR) {
            ret = 1;
        } else {
            ret = 0;
        }
    }

exit:
    return ret;
}
