# Rlyeh Lib

[[_TOC_]]

## Introduction

This Rlyeh library contains functions and variables that should be used by Ckopeo to pull/remove an oci image spec. It also offers functions to get informations from an oci image spec to R'lyeh.

It uses libcurl to download the oci image spec and libocispec to manipulate the downloaded files.

<!-- BEGIN-INTERNAL -->
## Building, installing and testing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    If you have no clue how to do this here are some links that could help you:

    - [Get Docker Engine - Community for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)  <br /><br />

    Make sure you user id is added to the docker group:

    ```
    sudo usermod -aG docker $USER
    ```

1. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/workspace/lcm
    ```

    Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx/:/home/$USER/workspace/lcm/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    If you are using vpn, you need to add `--dns 192.168.16.10 --dns 192.168.16.11` to the docker run command.  

    The `-v` option bind mounts the local directory for the lcm project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites

- [lib\_amxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [lib\_amxj](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj)
- [yajl](https://lloyd.github.io/yajl/)
- "libocispec" ---> NO LINK IN PUBLIC GITLAB : should I mention the public link to github repo ? https://github.com/containers/libocispec


#### Build librlyeh

1. Clone the git repository

    To be able to build it, you need the source code. So make a directory for the LCM and clone this library in it.

    ```bash
    mkdir -p ~/workspace/lcm/libraries
    cd ~/workspace/lcm/libraries
    git clone git@gitlab.com:prpl-foundation/lcm/libraries/librlyeh.git
    ```

1. Install dependencies

    Although the container will contain all tools needed for building, it does not contain all the libraries needed for building `librlyeh`. To be able to build `librlyeh` you need `libamxc` and libamxj. This library can be installed in the container.

    ```bash
    sudo apt update
    sudo apt install libamxc
    sudo apt install libamxj
    ```

    Note that you do not need to install all components explicitly. Some components will be installed automatically because the other components depend on them.

1. Build it

   In the container:

    ```bash
    cd ~/workspace/lcm/libraries/librlyeh
    make
    ```

### Installing

#### Using make target install

You can install your own compiled version easily in the container by running the install target.

```bash
cd ~/workspace/lcm/libraries/librlyeh
sudo -E make install
```
<!-- END-INTERNAL -->