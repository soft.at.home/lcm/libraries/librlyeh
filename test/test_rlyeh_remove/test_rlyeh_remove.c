/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include <amxc/amxc.h>
#include <debug/sahtrace.h>

#include <rlyeh/rlyeh.h>
#include <rlyeh/rlyeh_remove.h>
#include <rlyeh/rlyeh_utils.h>
#include "test_rlyeh_remove.h"

#define UNUSED __attribute__((unused))

static void copy_dir_content(const char* src, const char* dest) {
    DIR* dir;
    struct dirent* direntp;

    dir = opendir(src);
    if(dir) {
        while((direntp = readdir(dir)) != NULL) {
            if(CHECK_DIR_DOT_DOT(direntp->d_name)) {
                continue;
            }

            amxc_string_t src_fname;
            amxc_string_t dest_fname;
            amxc_string_init(&src_fname, 0);
            amxc_string_init(&dest_fname, 0);
            amxc_string_setf(&src_fname, "%s/%s", src, direntp->d_name);
            amxc_string_setf(&dest_fname, "%s/%s", dest, direntp->d_name);

            if(direntp->d_type == DT_DIR) {
                mkdir(dest_fname.buffer, 0755);
                copy_dir_content(src_fname.buffer, dest_fname.buffer);
            } else if((direntp->d_type == DT_REG) || (direntp->d_type == DT_LNK)) {
                rlyeh_copy_file(src_fname.buffer, dest_fname.buffer);
            }

            amxc_string_clean(&src_fname);
            amxc_string_clean(&dest_fname);
        }
        closedir(dir);
    }
}

void test_rlyeh_remove_blobs_dir(UNUSED void** state) {
    rlyeh_remove_data_t data;
    rlyeh_remove_data_init(&data);

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_remove_blobs_dir");

    mkdir("/tmp/test_rlyeh_remove_blobs_dir", 0755);
    mkdir("/tmp/test_rlyeh_remove_blobs_dir/images", 0755);
    mkdir("/tmp/test_rlyeh_remove_blobs_dir/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3", 0755);
    mkdir("/tmp/test_rlyeh_remove_blobs_dir/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/blobs", 0755);
    mkdir("/tmp/test_rlyeh_remove_blobs_dir/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/blobs/sha256", 0755);

    copy_dir_content("images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3",
                     "/tmp/test_rlyeh_remove_blobs_dir/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3");
    copy_dir_content("blobs/sha256",
                     "/tmp/test_rlyeh_remove_blobs_dir/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/blobs/sha256");

    data.imageLocation = strdup("/tmp/test_rlyeh_remove_blobs_dir/images");
    data.imageDiskLocation = strdup("/tmp/test_rlyeh_remove_blobs_dir/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3");
    data.version = strdup("latest");
    data.duid = strdup("duid3");

    SAH_TRACEZ_INFO(__func__, "Removing %s:%s:%s with blobs subdir", data.duid, data.version, data.imageDiskLocation);
    rlyeh_remove(&data);
    assert_false(file_exists("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/oci-layout"));
    assert_false(dir_exists("/tmp/test_rlyeh_remove_blobs_dir/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/blobs/sha256"));
    assert_false(dir_exists("/tmp/test_rlyeh_remove_blobs_dir/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/blobs"));
    assert_false(dir_exists("/tmp/test_rlyeh_remove_blobs_dir/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3"));
    rmdir("/tmp/test_rlyeh_remove_blobs_dir/images");

    mkdir("/tmp/test_rlyeh_remove_blobs_dir/blobs", 0755);
    mkdir("/tmp/test_rlyeh_remove_blobs_dir/blobs/sha256", 0755);
    mkdir("/tmp/test_rlyeh_remove_blobs_dir/blobs/sha512", 0755);
    mkdir("/tmp/test_rlyeh_remove_blobs_dir/blobs/esoterichashalgo", 0755);

    copy_dir_content("blobs/sha256", "/tmp/test_rlyeh_remove_blobs_dir/blobs/sha256");
    copy_dir_content("blobs/sha256", "/tmp/test_rlyeh_remove_blobs_dir/blobs/sha512");
    copy_dir_content("blobs/sha256", "/tmp/test_rlyeh_remove_blobs_dir/blobs/esoterichashalgo");

    rlyeh_remove_blobs_dir("/tmp/test_rlyeh_remove_blobs_dir/blobs");
    assert_false(dir_exists("/tmp/test_rlyeh_remove_blobs_dir/blobs"));

    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_remove_blobs_dir"));
    rmdir("/tmp/test_rlyeh_remove_blobs_dir");
    rlyeh_remove_data_clean(&data);
    sahTraceClose();
}

void test_rlyeh_remove(UNUSED void** state) {
    rlyeh_remove_data_t data;
    rlyeh_remove_data_init(&data);

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_remove");

    // set remove env in /tmp/test_rlyeh_remove/
    mkdir("/tmp/test_rlyeh_remove", 0755);
    mkdir("/tmp/test_rlyeh_remove/images", 0755);
    mkdir("/tmp/test_rlyeh_remove/images/image1", 0755);
    mkdir("/tmp/test_rlyeh_remove/images/image2", 0755);
    mkdir("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3", 0755);
    mkdir("/tmp/test_rlyeh_remove/images/somegroup1", 0755);
    mkdir("/tmp/test_rlyeh_remove/images/somegroup1/somegroup2", 0755);
    mkdir("/tmp/test_rlyeh_remove/images/somegroup1/somegroup2/somegroup3", 0755);
    mkdir("/tmp/test_rlyeh_remove/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4", 0755);

    mkdir("/tmp/test_rlyeh_remove/blobs", 0755);
    mkdir("/tmp/test_rlyeh_remove/blobs/sha256", 0755);

    copy_dir_content("images/image1", "/tmp/test_rlyeh_remove/images/image1");
    copy_dir_content("images/image2", "/tmp/test_rlyeh_remove/images/image2");
    copy_dir_content("images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3",
                     "/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3");
    copy_dir_content("blobs/sha256", "/tmp/test_rlyeh_remove/blobs/sha256");
    copy_dir_content("images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4",
                     "/tmp/test_rlyeh_remove/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4");

    data.imageLocation = strdup("/tmp/test_rlyeh_remove/images");
    data.imageDiskLocation = strdup("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3");
    data.version = strdup("latest");
    data.duid = strdup("duid9999");

    SAH_TRACEZ_INFO(__func__, "'Not' Removing %s:%s:%s", data.duid, data.version, data.imageDiskLocation);
    assert_true(file_exists("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/oci-layout"));

    free(data.duid);
    data.duid = strdup("duid3");
    free(data.version);
    data.version = strdup("latest999999");
    SAH_TRACEZ_INFO(__func__, "'Not' Removing %s:%s:%s", data.duid, data.version, data.imageDiskLocation);
    assert_true(file_exists("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/oci-layout"));

    free(data.version);
    data.version = strdup("latest");
    SAH_TRACEZ_INFO(__func__, "Removing %s:%s:%s", data.duid, data.version, data.imageDiskLocation);
    rlyeh_remove(&data);
    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_remove/blobs", "/tmp/test_rlyeh_remove/images");

    assert_false(file_exists("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3/oci-layout"));
    assert_false(dir_exists("/tmp/test_rlyeh_remove/images/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image3"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob9"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob10"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob11"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob12"));

    rlyeh_remove_data_clean(&data);
    rlyeh_remove_data_init(&data);
    data.imageLocation = strdup("/tmp/test_rlyeh_remove/images");
    data.imageDiskLocation = strdup("/tmp/test_rlyeh_remove/images/image1");
    data.version = strdup("latest");
    data.duid = strdup("duid1");

    SAH_TRACEZ_INFO(__func__, "Removing %s:%s:%s", data.duid, data.version, data.imageDiskLocation);
    rlyeh_remove(&data);

    assert_true(file_exists("/tmp/test_rlyeh_remove/images/image1/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/images/image1/oci-layout"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob1"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob3")); // <-- shared blob
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob4")); // <-- shared blob
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob7")); // <-- shared blob

    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_remove/blobs", "/tmp/test_rlyeh_remove/images");
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob1"));

    rlyeh_remove_data_clean(&data);
    rlyeh_remove_data_init(&data);
    data.imageLocation = strdup("/tmp/test_rlyeh_remove/images");
    data.imageDiskLocation = strdup("/tmp/test_rlyeh_remove/images/image2");
    data.version = strdup("latest");
    data.duid = strdup("duid2");

    SAH_TRACEZ_INFO(__func__, "Removing %s:%s:%s", data.duid, data.version, data.imageDiskLocation);
    rlyeh_remove(&data);
    assert_false(file_exists("/tmp/test_rlyeh_remove/images/image2/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/images/image2/oci-layout"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob2"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob5"));

    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_remove/blobs", "/tmp/test_rlyeh_remove/images");
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob2"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob5"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob6")); // <-- shared blob
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob7")); // <-- shared blob

    rlyeh_remove_data_clean(&data);
    rlyeh_remove_data_init(&data);
    data.imageLocation = strdup("/tmp/test_rlyeh_remove/images");
    data.imageDiskLocation = strdup("/tmp/test_rlyeh_remove/images/image1");
    data.version = strdup("v1");
    data.duid = strdup("duid1");

    SAH_TRACEZ_INFO(__func__, "Removing %s:%s:%s", data.duid, data.version, data.imageDiskLocation);
    rlyeh_remove(&data);
    assert_false(file_exists("/tmp/test_rlyeh_remove/images/image1/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/images/image1/oci-layout"));
    assert_false(dir_exists("/tmp/test_rlyeh_remove/images/image1"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob8"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob3"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob4")); // <-- shared blob
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob6")); // <-- shared blob

    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_remove/blobs", "/tmp/test_rlyeh_remove/images");
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob8"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob3"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob4")); // <-- shared blob
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob6")); // <-- shared blob

    rlyeh_remove_data_clean(&data);
    rlyeh_remove_data_init(&data);
    data.imageLocation = strdup("/tmp/test_rlyeh_remove/images");
    data.imageDiskLocation = strdup("/tmp/test_rlyeh_remove/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4");
    data.version = strdup("latest");
    data.duid = strdup("duid4");

    SAH_TRACEZ_INFO(__func__, "Removing %s:%s:%s", data.duid, data.version, data.imageDiskLocation);
    rlyeh_remove(&data);
    assert_false(file_exists("/tmp/test_rlyeh_remove/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4/oci-layout"));
    assert_false(dir_exists("/tmp/test_rlyeh_remove/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4"));
    assert_false(dir_exists("/tmp/test_rlyeh_remove/images/somegroup1"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob13"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob14"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob15"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob16"));
    assert_true(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob7")); // <-- shared blob

    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_remove/blobs", "/tmp/test_rlyeh_remove/images");

    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob13"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob14"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob15"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob16"));
    assert_false(file_exists("/tmp/test_rlyeh_remove/blobs/sha256/blob7")); // <-- shared blob

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_remove/images");
    rmdir("/tmp/test_rlyeh_remove/images");

    rlyeh_remove_blobs_dir("/tmp/test_rlyeh_remove/blobs");
    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_remove"));
    rmdir("/tmp/test_rlyeh_remove");

    rlyeh_remove_data_clean(&data);
    sahTraceClose();
}

void test_rlyeh_remove_unlisted_blobs(UNUSED void** state) {
    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_remove_unlisted_blobs");

    // set remove env in /tmp/test_rlyeh/
    mkdir("/tmp/test_rlyeh_remove_unlisted_blobs", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_blobs/images", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_blobs/images/image1", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_blobs/images/somegroup1", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_blobs/images/somegroup1/somegroup2", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_blobs/images/somegroup1/somegroup2/somegroup3", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_blobs/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4", 0755);

    mkdir("/tmp/test_rlyeh_remove_unlisted_blobs/blobs", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256", 0755);

    copy_dir_content("images/image1", "/tmp/test_rlyeh_remove_unlisted_blobs/images/image1");
    copy_dir_content("blobs/sha256", "/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256");
    copy_dir_content("images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4",
                     "/tmp/test_rlyeh_remove_unlisted_blobs/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4");

    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_remove_unlisted_blobs/blobs", "/tmp/test_rlyeh_remove_unlisted_blobs/images");
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob2"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob5"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob9"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob10"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob11"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob12"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob1"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob3"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob4"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob13"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob14"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob15"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob16"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256/blob7"));

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_remove_unlisted_blobs/images");
    rmdir("/tmp/test_rlyeh_remove_unlisted_blobs/images");

    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_remove_unlisted_blobs/blobs", NULL);
    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256"));
    assert_false(rlyeh_is_empty_dir("/tmp/test_rlyeh_remove_unlisted_blobs/blobs"));
    rmdir("/tmp/test_rlyeh_remove_unlisted_blobs/blobs/sha256");
    rmdir("/tmp/test_rlyeh_remove_unlisted_blobs/blobs");

    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_remove_unlisted_blobs"));
    rmdir("/tmp/test_rlyeh_remove_unlisted_blobs");

    sahTraceClose();
}

void test_rlyeh_remove_unlisted_images(UNUSED void** state) {
    int retval = 0;
    rlyeh_remove_data_t* keep_image;
    keep_image = rlyeh_remove_data_arr_allocate(10);

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_remove_unlisted_imagespec");

    // set remove env in /tmp/test_rlyeh/
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/images", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/images/1imageshadowed", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1shadowed", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image2", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/images/somegroup1", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/images/somegroup1/somegroup2", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/images/somegroup1/somegroup2/somegroup3", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4", 0755);
    // mkdir("/tmp/test_rlyeh/images/image3", 0755);

    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs", 0755);
    mkdir("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256", 0755);

    copy_dir_content("images/image1", "/tmp/test_rlyeh_remove_unlisted_imagespec/images/1imageshadowed");
    copy_dir_content("images/image1", "/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1");
    copy_dir_content("images/image1", "/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1shadowed");
    copy_dir_content("images/image2", "/tmp/test_rlyeh_remove_unlisted_imagespec/images/image2");
    copy_dir_content("blobs/sha256", "/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256");
    copy_dir_content("images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4",
                     "/tmp/test_rlyeh_remove_unlisted_imagespec/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4");

    keep_image[0].duid = strdup("duid1");
    keep_image[0].version = strdup("v1");
    keep_image[0].imageDiskLocation = strdup("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1");

    retval = rlyeh_remove_unlisted_images("/tmp/test_rlyeh_remove_unlisted_imagespec/images", keep_image, 1);
    assert_int_equal(retval, 0);
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/images/1imageshadowed/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/images/1imageshadowed/oci-layout"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1shadowed/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1shadowed/oci-layout"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1/oci-layout"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image2/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image2/oci-layout"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob1"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob3"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob4"));
    assert_true(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob7"));
    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs", "/tmp/test_rlyeh_remove_unlisted_imagespec/images");
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob1"));

    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob7"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob2"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob5"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/images/somegroup1/somegroup2/somegroup3/very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_name_image4/oci-layout"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob13"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob14"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob15"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob16"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob7"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs/sha256/blob9"));

    retval = rlyeh_remove_unlisted_images("/tmp/test_rlyeh_remove_unlisted_imagespec/images", NULL, 0);
    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_remove_unlisted_imagespec/images/"));
    rmdir("/tmp/test_rlyeh_remove_unlisted_imagespec/images");
    rlyeh_remove_blobs_dir("/tmp/test_rlyeh_remove_unlisted_imagespec/blobs");
    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_remove_unlisted_imagespec"));
    rmdir("/tmp/test_rlyeh_remove_unlisted_imagespec");

    keep_image[9].duid = strdup("duid1");
    keep_image[9].version = strdup("v1");
    keep_image[9].imageDiskLocation = strdup("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1");

    keep_image[5].duid = strdup("duid1");
    keep_image[5].version = strdup("v1");
    keep_image[5].imageDiskLocation = strdup("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image1");

    keep_image[2].duid = strdup("duid1");
    keep_image[3].version = strdup("version");
    keep_image[4].imageDiskLocation = strdup("/tmp/test_rlyeh_remove_unlisted_imagespec/images/image5");

    rlyeh_remove_data_arr_clean(keep_image, 10);
    free(keep_image);
    sahTraceClose();
}

void test_rlyeh_remove_invalid_images(UNUSED void** state) {
    int retval = 0;
    rlyeh_remove_data_t* keep_image;
    keep_image = rlyeh_remove_data_arr_allocate(2);

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_remove_invalid_imagespec");

    // set remove env in /tmp/test_rlyeh/
    mkdir("/tmp/test_rlyeh_remove_invalid_imagespec", 0755);
    mkdir("/tmp/test_rlyeh_remove_invalid_imagespec/images", 0755);
    mkdir("/tmp/test_rlyeh_remove_invalid_imagespec/images/image1", 0755);
    mkdir("/tmp/test_rlyeh_remove_invalid_imagespec/images/invalid", 0755);
    mkdir("/tmp/test_rlyeh_remove_invalid_imagespec/images/group1", 0755);
    mkdir("/tmp/test_rlyeh_remove_invalid_imagespec/images/group1/group2", 0755);
    mkdir("/tmp/test_rlyeh_remove_invalid_imagespec/images/group1/group2/invalid2", 0755);

    copy_dir_content("images/image1", "/tmp/test_rlyeh_remove_invalid_imagespec/images/image1");
    copy_dir_content("images/invalid", "/tmp/test_rlyeh_remove_invalid_imagespec/images/invalid");
    copy_dir_content("images/invalid", "/tmp/test_rlyeh_remove_invalid_imagespec/images/group1/group2/invalid2");

    keep_image[0].duid = strdup("duid1");
    keep_image[0].version = strdup("v1");
    keep_image[0].imageDiskLocation = strdup("/tmp/test_rlyeh_remove_invalid_imagespec/images/image1");
    keep_image[1].duid = strdup("invalid");
    keep_image[1].version = strdup("v1");
    keep_image[1].imageDiskLocation = strdup("/tmp/test_rlyeh_remove_invalid_imagespec/images/invalid");

    retval = rlyeh_remove_unlisted_images("/tmp/test_rlyeh_remove_invalid_imagespec/images", keep_image, 2);
    assert_int_equal(retval, 0);
    assert_false(file_exists("/tmp/test_rlyeh_remove_invalid_imagespec/images/invalid/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_remove_invalid_imagespec/images/group1/group2/invalid2/index.json"));

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_remove_invalid_imagespec/images");
    rmdir("/tmp/test_rlyeh_remove_invalid_imagespec/images");
    rlyeh_remove_blobs_dir("/tmp/test_rlyeh_remove_invalid_imagespec/blobs");
    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_remove_invalid_imagespec"));
    rmdir("/tmp/test_rlyeh_remove_invalid_imagespec");

    sahTraceClose();
    rlyeh_remove_data_arr_clean(keep_image, 2);
    free(keep_image);
}
