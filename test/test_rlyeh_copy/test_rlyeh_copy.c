/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include <amxc/amxc.h>
#include <rlyeh/rlyeh.h>
#include <rlyeh_copy_priv.h>

#include <debug/sahtrace.h>

#include "test_rlyeh_copy.h"

#define UNUSED __attribute__((unused))

static inline void print_rlyeh_copy_result(rlyeh_status_t s, const char* e) {
    SAH_TRACEZ_NOTICE(__func__, "rlyeh_copy returned %d", (s));
    if(s) {
        SAH_TRACEZ_NOTICE(__func__, "err_msg: %s", (e));
    }
}

int test_rlyeh_copy_setup(UNUSED void** state) {
    return rlyeh_curl_init();
}
int test_rlyeh_copy_teardown(UNUSED void** state) {
    rlyeh_curl_cleanup();
    return 0;
}

static int check_image_in_index_manifest(const char* filepath, const char* version, const char* duid, size_t* it) {
    int ret;
    char* err_msg = NULL;
    image_spec_schema_image_index_schema* index_schema = image_spec_schema_image_index_schema_parse_file(filepath, 0, &err_msg);
    if(index_schema == NULL) {
        SAH_TRACEZ_ERROR(__func__, "Couldnt parse %s: %s", filepath, err_msg);
        free(err_msg);
        return -1;
    }
    ret = rlyeh_get_it_from_index(it, index_schema, version, duid);
    free_image_spec_schema_image_index_schema(index_schema);
    return ret;
}

// ATM just checks for any blob to be symlinked
// as its hard to determine which are the rootfs blobs and which are not without a lot of code
static int check_if_ro_blobs_are_symlinked(const rlyeh_copy_data_t* data) {
    int ret = -1;
    struct dirent* de;
    DIR* dr;

    dr = opendir(data->ro_shared_blob_dir);
    if(dr != NULL) {
        SAH_TRACEZ_INFO(__func__, "Checking RO blob store: %s", data->ro_shared_blob_dir);
        amxc_string_t blobsdir;
        amxc_string_init(&blobsdir, 0);
        while((de = readdir(dr)) != NULL) {
            if(CHECK_DIR_DOT_DOT(de->d_name)) {
                continue;
            }

            if((de->d_type == DT_DIR) || (de->d_type == DT_LNK)) {
                struct dirent* sub_de;
                DIR* sub_dr;

                amxc_string_setf(&blobsdir, "%s/%s", data->ro_shared_blob_dir, de->d_name);
                sub_dr = opendir(blobsdir.buffer);
                if(sub_dr != NULL) {
                    SAH_TRACEZ_INFO(__func__, "Checking Hash dir in RO blob store: %s", blobsdir.buffer);
                    amxc_string_t link_name;
                    amxc_string_init(&link_name, 0);

                    while((sub_de = readdir(sub_dr)) != NULL) {
                        if(CHECK_DIR_DOT_DOT(sub_de->d_name)) {
                            continue;
                        }
                        if((sub_de->d_type == DT_REG) || (sub_de->d_type == DT_LNK)) {
                            SAH_TRACEZ_INFO(__func__, "Checking %s:%s from ro blob store with rw blob store in %s", de->d_name, sub_de->d_name, data->dest_shared_blob_dir);
                            amxc_string_setf(&link_name, "%s/%s/%s", data->dest_shared_blob_dir, de->d_name, sub_de->d_name);
                            switch(rlyeh_file_is_symlink(link_name.buffer)) {
                            case 1:
                                SAH_TRACEZ_INFO(__func__, "Blob is confirmed symlink");
                                ret = 0;
                                break;
                            case 0:
                                SAH_TRACEZ_INFO(__func__, "Blob exists but not a symlink");
                                break;
                            default:
                                SAH_TRACEZ_ERROR(__func__, "Failed checking for %s:%s in rw blob store: (%d) %s", de->d_name, sub_de->d_name, errno, strerror(errno));
                                break;
                            }
                        }
                    }
                    amxc_string_clean(&link_name);
                    closedir(sub_dr);
                }
            }
        }
        amxc_string_clean(&blobsdir);
        closedir(dr);
    }

    return ret;
}

void test_rlyeh_ro_blob_store(UNUSED void** state) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    rlyeh_copy_data_t data;
    char err_msg[RLYEH_ERR_MSG_LEN] = "";
    int retval = 0;
    size_t it_index;

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_ro_blob_store");

    // Create images and blobs dir
    mkdir("/tmp/test_rlyeh_ro_blob_store", 0755);
    mkdir("/tmp/test_rlyeh_ro_blob_store/images", 0755);
    mkdir("/tmp/test_rlyeh_ro_blob_store/blobs", 0755);
    mkdir("/tmp/test_rlyeh_ro_blob_store/ro_blobs", 0755);

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_copy");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "rlyeh_images");
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, __func__);
    sahTraceAddZone(500, "check_if_ro_blobs_are_symlinked");
    sahTraceAddZone(500, "check_image_in_index_manifest");
    sahTraceAddZone(500, "print_rlyeh_copy_result");
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    // Data initialization
    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine:latest");
    data.destination = strdup("oci:///tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine:latest");
    data.images_location = strdup("/tmp/test_rlyeh_ro_blob_store/images");
    data.duid = strdup("alpine");

    SAH_TRACEZ_INFO(__func__, "Populating ro_blob store");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_ro_blob_store/ro_blobs");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, 0);
    assert_true(file_exists("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/index.json", "latest", "alpine", &it_index);
    assert_int_equal(retval, 0);
    remove("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/index.json");
    remove("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/oci-layout");
    assert_string_equal(data.version, "latest");
    free(data.version);

    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, 0);
    assert_true(file_exists("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/index.json", "latest", "alpine", &it_index);
    assert_int_equal(retval, 0);
    remove("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/index.json");
    remove("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/oci-layout");
    assert_string_equal(data.version, "latest");
    free(data.version);

    SAH_TRACEZ_INFO(__func__, "Trying RO blob store");
    free(data.dest_shared_blob_dir);
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_ro_blob_store/blobs");
    data.ro_shared_blob_dir = strdup("/tmp/test_rlyeh_ro_blob_store/ro_blobs");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, 0);
    assert_true(file_exists("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/index.json", "latest", "alpine", &it_index);
    assert_int_equal(retval, 0);
    retval = check_if_ro_blobs_are_symlinked(&data);
    assert_int_equal(retval, 0);

    rlyeh_copy_data_clean(&data);

    remove("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/index.json");
    remove("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine/oci-layout");

    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_ro_blob_store/blobs", "/tmp/test_rlyeh_ro_blob_store/images");
    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_ro_blob_store/ro_blobs", "/tmp/test_rlyeh_ro_blob_store/images");

    rmdir("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers/alpine");
    rmdir("/tmp/test_rlyeh_ro_blob_store/images/lcmtestcontainers");
    rmdir("/tmp/test_rlyeh_ro_blob_store/images");
    rmdir("/tmp/test_rlyeh_ro_blob_store/blobs/sha256");
    rmdir("/tmp/test_rlyeh_ro_blob_store/blobs");
    rmdir("/tmp/test_rlyeh_ro_blob_store/ro_blobs/sha256");
    rmdir("/tmp/test_rlyeh_ro_blob_store/ro_blobs");
    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_ro_blob_store"));
    rmdir("/tmp/test_rlyeh_ro_blob_store");

    sahTraceClose();
}

void test_rlyeh_copy(UNUSED void** state) {
    size_t it_index = 0;
    int retval = 0;
    rlyeh_status_t status = RLYEH_NO_ERROR;
    rlyeh_copy_data_t data;
    char err_msg[RLYEH_ERR_MSG_LEN] = "";

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_copy");

    // Create images and blobs dir
    mkdir("/tmp/test_rlyeh_copy", 0755);
    mkdir("/tmp/test_rlyeh_copy/images", 0755);
    mkdir("/tmp/test_rlyeh_copy/blobs", 0755);

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_copy");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "rlyeh_images");
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "print_rlyeh_copy_result");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    // Data initialization
    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine:latest");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine:latest");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    data.duid = strdup("alpine1");

    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine/index.json", "latest", "alpine1", &it_index);
    assert_int_equal(retval, 0);
    assert_string_equal(data.version, "latest");

    free(data.duid);
    free(data.version);
    data.duid = strdup("alpine2");
    // Test image already exists
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine/index.json", "latest", "alpine2", &it_index);
    assert_int_equal(retval, 0);
    rlyeh_copy_data_clean(&data);

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated_test:3.19");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_annotated:3.19");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    data.duid = strdup("alpine_annotated1");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_annotated/index.json", "3.19", "alpine_annotated1", &it_index);
    assert_int_equal(retval, 0);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/4abcf20661432fb2d719aaf90656f55c287f8ca915dc1c92ec14ff61e67fbaf8"));
    assert_string_equal(data.version, "3.19");
    rlyeh_copy_data_clean(&data);

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated_test:3.18");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_annotated:3.18");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    data.duid = strdup("alpine_annotated2");

    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_annotated/index.json", "3.18", "alpine_annotated2", &it_index);
    assert_int_equal(retval, 0);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/619be1103602d98e1963557998c954c892b3872986c27365e9f651f5bc27cab8"));
    assert_string_equal(data.version, "3.18");

    rlyeh_copy_data_clean(&data);

    // Test with wrong format in destination
    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine_modified_annotated_test:3.18");
    data.destination = strdup("docker:///tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated:3.18");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("alpine_modify_annotated_wrong_dest");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, 2);  // <-- invalid arguments
    assert_false(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json", "latest", "alpine_modify_annotated_wrong_dest", &it_index);
    assert_int_not_equal(retval, 0);
    rlyeh_copy_data_clean(&data);
    assert_false(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/fa7a6596eabc7cfeb36a1856bc09e904bd5acab1c9f35e4f696586f59de2ddc6"));

    // Test with wrong credentials
    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/ubuntu_private_modified_annotated:priv_20.04");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated:20.04");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.username = strdup("user");
    data.password = strdup("password");
    data.duid = strdup("ubuntu_private_modified_annotated_wrong_creds");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, 5);  // <-- download failed due to wrong credentials
    assert_false(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/oci-layout"));
    rlyeh_copy_data_clean(&data);
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/index.json", "20.04", "ubuntu_private_modified_annotated_wrong_creds", &it_index);
    assert_int_not_equal(retval, 0);
    assert_false(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/17d0386c2fff30a5b92652bbef2b84639dba9b9f17bdbb819c8d10badd827fdb"));

    // Test without server in uri
    // annd test without explicitly setting the creds
    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/ubuntu_private_modified_annotated:priv_20.04");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated:20.04");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("ubuntu_private_modified_annotated1");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/oci-layout"));
    assert_string_equal(data.version, "20.04");
    rlyeh_copy_data_clean(&data);
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/index.json", "20.04", "ubuntu_private_modified_annotated1", &it_index);
    assert_int_equal(retval, 0);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/1c258fc195c5eca1b98a1da5d62eabdefcfb077fcac9369db286ed1458789d52"));

    // Test without server in uri
    // annd test with explicitly setting the creds
    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/ubuntu_private_modified_annotated:priv_18.04");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated:18.04");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("ubuntu_private_modified_annotated2");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    data.username = strdup("lcmtestcontainers");
    data.password = strdup("A6Kdw2UyiLnYmzI6b9kO");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/oci-layout"));
    assert_string_equal(data.version, "18.04");
    rlyeh_copy_data_clean(&data);
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/index.json", "18.04", "ubuntu_private_modified_annotated2", &it_index);
    assert_int_equal(retval, 0);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/56fb0d545b658ff73430b5d44912bb8179f0f8abfbf34fc4a90ab18b9307520a"));

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/alpine_modified_annotated_test:3.18");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated:3.18");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("alpine_modified_annotated1");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json", "3.18", "alpine_modified_annotated1", &it_index);
    assert_int_equal(retval, 0);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/fa7a6596eabc7cfeb36a1856bc09e904bd5acab1c9f35e4f696586f59de2ddc6"));
    assert_string_equal(data.version, "3.18");
    rlyeh_copy_data_clean(&data);

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/alpine_modified_annotated_test:3.18");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated:3.18");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("alpine_modified_annotated2");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json", "3.18", "alpine_modified_annotated2", &it_index);
    assert_int_equal(retval, 0);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/fa7a6596eabc7cfeb36a1856bc09e904bd5acab1c9f35e4f696586f59de2ddc6"));
    assert_string_equal(data.version, "3.18");
    rlyeh_copy_data_clean(&data);

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/alpine_modified_annotated_test:3.19");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated:3.19");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("alpine_modified_annotated1");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json", "3.19", "alpine_modified_annotated1", &it_index);
    assert_int_equal(retval, 0);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/a0bed814693a2470db647d234b2a32f9b31153fffa71e1dad78db9166a1428a4"));
    assert_string_equal(data.version, "3.19");

    free(data.version);
    // Test avoid re-download blobs by handfully removing the image (by rm'ing the index)
    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json");
    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/oci-layout");
    rmdir("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated");

    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json", "3.19", "alpine_modified_annotated1", &it_index);
    assert_int_equal(retval, 0);
    assert_string_equal(data.version, "3.19");
    rlyeh_copy_data_clean(&data);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/a0bed814693a2470db647d234b2a32f9b31153fffa71e1dad78db9166a1428a4"));

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/ubuntu_annotated_test:22.04");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated:22.04");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("ubuntu_annotated1");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated/index.json", "22.04", "ubuntu_annotated1", &it_index);
    assert_int_equal(retval, 0);
    assert_string_equal(data.version, "22.04");
    rlyeh_copy_data_clean(&data);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/bccd10f490ab0f3fba61b193d1b80af91b17ca9bdca9768a16ed05ce16552fcb"));

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/ubuntu_annotated_test:24.04");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated:24.04");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("ubuntu_annotated1");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated/index.json", "24.04", "ubuntu_annotated1", &it_index);
    assert_int_equal(retval, 0);
    assert_string_equal(data.version, "24.04");
    rlyeh_copy_data_clean(&data);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/26489f2f1781332c4e802d1dce0cf4978a1b210b92984bd4183dd65a12c163b4"));

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/ubuntu_modified_annotated_test:22.04");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated:22.04");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("ubuntu_modified_annotated1");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated/index.json", "22.04", "ubuntu_modified_annotated1", &it_index);
    assert_int_equal(retval, 0);
    assert_string_equal(data.version, "22.04");
    rlyeh_copy_data_clean(&data);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/0fb1184d526b54d7f3de2cd986040b1917f596b17ca221433dca0f21c0657407"));

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/ubuntu_modified_annotated_test:24.04");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated:24.04");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("ubuntu_modified_annotated1");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated/index.json", "24.04", "ubuntu_modified_annotated1", &it_index);
    assert_int_equal(retval, 0);
    assert_string_equal(data.version, "24.04");
    rlyeh_copy_data_clean(&data);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/cd88210593112454c7b2e1fc642e55f36b19dd7d37a16270c0013da83b20aaba"));

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/alpine_modified_annotated_test:3.18");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated:3.18");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("alpine_modified_annotated2");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json", "3.18", "alpine_modified_annotated2", &it_index);
    assert_int_equal(retval, 0);
    assert_string_equal(data.version, "3.18");
    rlyeh_copy_data_clean(&data);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/fa7a6596eabc7cfeb36a1856bc09e904bd5acab1c9f35e4f696586f59de2ddc6"));

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://lcmtestcontainers/alpine_modified_annotated_test:3.19");
    data.destination = strdup("oci:///tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated:3.19");
    data.dest_shared_blob_dir = strdup("/tmp/test_rlyeh_copy/blobs");
    data.duid = strdup("alpine_modified_annotated3");
    data.images_location = strdup("/tmp/test_rlyeh_copy/images");
    status = rlyeh_copy(&data, err_msg);
    print_rlyeh_copy_result(status, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/oci-layout"));
    retval = check_image_in_index_manifest("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json", "3.19", "alpine_modified_annotated3", &it_index);
    assert_int_equal(retval, 0);
    assert_string_equal(data.version, "3.19");
    rlyeh_copy_data_clean(&data);
    assert_true(file_exists("/tmp/test_rlyeh_copy/blobs/sha256/a0bed814693a2470db647d234b2a32f9b31153fffa71e1dad78db9166a1428a4"));

    // Remove images and blobs
    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine/index.json");
    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine/oci-layout");
    rmdir("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine");
    rmdir("/tmp/test_rlyeh_copy/images/lcmtestcontainers");

    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_annotated/index.json");
    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_annotated/oci-layout");
    rmdir("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_annotated");

    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/index.json");
    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated/oci-layout");
    rmdir("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_private_modified_annotated");

    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/index.json");
    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated/oci-layout");
    rmdir("/tmp/test_rlyeh_copy/images/lcmtestcontainers/alpine_modified_annotated");

    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated/index.json");
    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated/oci-layout");
    rmdir("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_annotated");

    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated/index.json");
    remove("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated/oci-layout");
    rmdir("/tmp/test_rlyeh_copy/images/lcmtestcontainers/ubuntu_modified_annotated");

    rmdir("/tmp/test_rlyeh_copy/images/lcmtestcontainers");

    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh_copy/blobs", "/tmp/test_rlyeh_copy/images");

    rmdir("/tmp/test_rlyeh_copy/blobs/sha256");

    rmdir("/tmp/test_rlyeh_copy/images");
    rmdir("/tmp/test_rlyeh_copy/blobs");
    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_copy"));
    rmdir("/tmp/test_rlyeh_copy");

    sahTraceClose();
}

void test_rlyeh_size_check(UNUSED void** state) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    char err_msg[RLYEH_ERR_MSG_LEN] = {};

    status = rlyeh_size_check(1024, 1024, "digest", err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);

    status = rlyeh_size_check(0, 1024, "digest", err_msg);
    assert_int_equal(status, RLYEH_ERROR_SIZE_CHECK);
}

void test_rlyeh_set_tmp_blob_filename(UNUSED void** state) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    amxc_string_t filename;
    amxc_string_init(&filename, 0);

    rlyeh_copy_output_directories_t output_dir;
    rlyeh_copy_output_directories_init(&output_dir);
    amxc_string_set(&output_dir.blobs, "blobs");
    amxc_string_set(&output_dir.image, "image");

    status = set_tmp_blob_filename("sha256:hash", &output_dir, &filename, NULL);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_string_equal(filename.buffer, "blobs/sha256/hash.tmp");

    rlyeh_copy_output_directories_clean(&output_dir);
    amxc_string_clean(&filename);
}

void test_rlyeh_get_authenticate_element(UNUSED void** state) {
    char* realm = NULL;
    char* service = NULL;
    char* scope = NULL;

    const char correct_buf[1024] = "HTTP/1.1 401 Unauthorized\r\ncontent-type: application/json\r\ndocker-distribution-api-version: registry/2.0\r\nwww-authenticate: Bearer realm=\"https://auth.docker.io/token\",service=\"registry.docker.io\",scope=\"repository:lcmtestcontainers/alpine:pull\"\r\ndate: Tue, 07 Dec 2021 09:32:08 GMT\r\ncontent-length: 157\r\nstrict-transport-security: max-age=31536000\r\ndocker-ratelimit-source: 84.198.211.186\r\n";
    get_authenticate_element(correct_buf, &realm, &service, &scope);
    assert_string_equal(realm, "https://auth.docker.io/token");
    assert_string_equal(service, "registry.docker.io");
    assert_string_equal(scope, "repository:lcmtestcontainers/alpine:pull");
    if(realm) {
        free(realm);
        realm = NULL;
    }
    if(service) {
        free(service);
        service = NULL;

    }
    if(scope) {
        free(scope);
        scope = NULL;

    }

    const char noauth_buf[1024] = "HTTP/1.1 401 Unauthorized\r\ncontent-type: application/json\r\ndocker-distribution-api-version: registry/2.0\r\ndate: Tue, 07 Dec 2021 09:32:08 GMT\r\ncontent-length: 157\r\nstrict-transport-security: max-age=31536000\r\ndocker-ratelimit-source: 84.198.211.186\r\n";
    get_authenticate_element(noauth_buf, &realm, &service, &scope);
    assert_null(realm);
    assert_null(service);
    assert_null(scope);
    if(realm) {
        free(realm);
    }
    if(service) {
        free(service);
    }
    if(scope) {
        free(scope);
    }
}

void test_rlyeh_get_creds_from_cmd(UNUSED void** state) {
    int retval;
    rlyeh_copy_data_t copy_data;

    rlyeh_copy_data_init(&copy_data);
    retval = rlyeh_get_creds_from_cmd(&copy_data, "usr:pwd");
    assert_int_equal(retval, 0);
    assert_string_equal(copy_data.username, "usr");
    assert_string_equal(copy_data.password, "pwd");
    rlyeh_copy_data_clean(&copy_data);

    rlyeh_copy_data_init(&copy_data);
    retval = rlyeh_get_creds_from_cmd(&copy_data, "username:");
    assert_int_equal(retval, -1);
    assert_string_equal(copy_data.username, "username");
    assert_null(copy_data.password);
    rlyeh_copy_data_clean(&copy_data);

    rlyeh_copy_data_init(&copy_data);
    retval = rlyeh_get_creds_from_cmd(&copy_data, "");
    assert_int_equal(retval, -2);
    assert_null(copy_data.username);
    assert_null(copy_data.password);
    rlyeh_copy_data_clean(&copy_data);
}
