/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <rlyeh/rlyeh.h>

#include <debug/sahtrace.h>

#include "test_rlyeh_utils.h"

#define UNUSED __attribute__((unused))

// Returns true if the files are the same.
static bool compareFile(const char* fname1, const char* fname2) {
    bool ret = false;
    char ch1, ch2;
    FILE* fd1 = fopen(fname1, "r");
    FILE* fd2 = fopen(fname2, "r");

    if((fd1 == NULL) || (fd2 == NULL)) {
        return false;
    }

    do {
        ch1 = fgetc(fd1);
        ch2 = fgetc(fd2);
        if(ch1 != ch2) {
            ret = false;
            goto exit;
        }
    } while(ch1 != EOF && ch2 != EOF);

    if((ch1 == EOF) && (ch2 == EOF)) {
        ret = true;
    } else {
        ret = false;
    }

exit:
    if(fd1) {
        fclose(fd1);
    }
    if(fd2) {
        fclose(fd2);
    }

    return ret;
}

void test_rlyeh_parse_uri(UNUSED void** state) {
    int retval;
    rlyeh_image_parameters_t uri_elements;

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("transport://image", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "transport");
    assert_string_equal(uri_elements.image_name, "image");
    assert_null(uri_elements.server);
    assert_string_equal(uri_elements.version, "latest");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("transport://image:version", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "transport");
    assert_string_equal(uri_elements.image_name, "image");
    assert_null(uri_elements.server);
    assert_string_equal(uri_elements.version, "version");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("transport://group/image:version", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "transport");
    assert_string_equal(uri_elements.image_name, "group/image");
    assert_null(uri_elements.server);
    assert_string_equal(uri_elements.version, "version");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("transport://group/image", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "transport");
    assert_string_equal(uri_elements.image_name, "group/image");
    assert_null(uri_elements.server);
    assert_string_equal(uri_elements.version, "latest");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("transport://server.com/alpine", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "transport");
    assert_string_equal(uri_elements.server, "server.com");
    assert_string_equal(uri_elements.image_name, "alpine");
    assert_string_equal(uri_elements.version, "latest");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("transport://server.com/image_name:version", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "transport");
    assert_string_equal(uri_elements.server, "server.com");
    assert_string_equal(uri_elements.image_name, "image_name");
    assert_string_equal(uri_elements.version, "version");
    rlyeh_image_parameters_clean(&uri_elements);


    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("docker://docker.v3d.fr/embedded-gui-hgw-generic-webui:1.2.2", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "docker");
    assert_string_equal(uri_elements.server, "docker.v3d.fr");
    assert_string_equal(uri_elements.image_name, "embedded-gui-hgw-generic-webui");
    assert_string_equal(uri_elements.version, "1.2.2");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("oci://path/image_name", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "oci");
    assert_string_equal(uri_elements.image_dir, "path/image_name");
    assert_string_equal(uri_elements.version, "latest");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("oci://path/image_name:version", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "oci");
    assert_string_equal(uri_elements.image_dir, "path/image_name");
    assert_string_equal(uri_elements.version, "version");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("oci://path/image_name:v5", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "oci");
    assert_string_equal(uri_elements.image_dir, "path/image_name");
    assert_string_equal(uri_elements.version, "v5");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("oci:///path/image_name_something:v5", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "oci");
    assert_string_equal(uri_elements.image_dir, "/path/image_name_something");
    assert_string_equal(uri_elements.version, "v5");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("oci:///path/image_name:v7", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "oci");
    assert_string_equal(uri_elements.image_dir, "/path/image_name");
    assert_string_equal(uri_elements.version, "v7");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("oci:///path/image_name", &uri_elements);
    assert_int_equal(retval, 0);
    assert_string_equal(uri_elements.transport, "oci");
    assert_string_equal(uri_elements.image_dir, "/path/image_name");
    assert_string_equal(uri_elements.version, "latest");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri(NULL, &uri_elements);
    assert_int_equal(retval, -2);
    assert_null(uri_elements.transport);
    assert_null(uri_elements.image_name);
    assert_null(uri_elements.version);
    assert_null(uri_elements.server);
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("docker://docker.v3d.fr/embedded-gui-hgw-generic-webui:1.2.2", NULL);
    assert_int_equal(retval, -2);
    assert_null(uri_elements.transport);
    assert_null(uri_elements.image_name);
    assert_null(uri_elements.version);
    assert_null(uri_elements.server);
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri(NULL, &uri_elements);
    assert_int_equal(retval, -2);
    assert_null(uri_elements.transport);
    assert_null(uri_elements.image_name);
    assert_null(uri_elements.version);
    assert_null(uri_elements.server);
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("", &uri_elements);
    assert_int_equal(retval, -1);
    assert_null(uri_elements.transport);
    assert_null(uri_elements.image_name);
    assert_null(uri_elements.version);
    assert_null(uri_elements.server);
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("oci://", &uri_elements);
    assert_int_equal(retval, -1);
    assert_null(uri_elements.transport);
    assert_null(uri_elements.image_name);
    assert_null(uri_elements.version);
    assert_null(uri_elements.server);
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    retval = rlyeh_parse_uri("oci", &uri_elements);
    assert_int_equal(retval, -1);
    assert_null(uri_elements.transport);
    assert_null(uri_elements.image_name);
    assert_null(uri_elements.version);
    assert_null(uri_elements.server);
    rlyeh_image_parameters_clean(&uri_elements);
}

void test_build_manifest_filename_from_index(UNUSED void** state) {
    bool ret = false;

    parser_error err = NULL;
    amxc_string_t fname;
    amxc_string_init(&fname, 0);
    image_spec_schema_image_index_schema* index_schema = NULL;

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    index_schema = image_spec_schema_image_index_schema_parse_file("index.json", 0, &err);
    assert_non_null(index_schema);
    ret = build_manifest_filename_from_index(index_schema, "latest", "duid1", "blobs", &fname);
    assert_false(ret);
    assert_string_equal("blobs/sha256/blob1", fname.buffer);
    free_image_spec_schema_image_index_schema(index_schema);
    amxc_string_clean(&fname);

    index_schema = image_spec_schema_image_index_schema_parse_file("index.json", 0, &err);
    assert_non_null(index_schema);
    ret = build_manifest_filename_from_index(index_schema, "version2", "duid3", "blobs", &fname);
    assert_false(ret);
    assert_string_equal("blobs/sha256/blob3", fname.buffer);
    free_image_spec_schema_image_index_schema(index_schema);
    amxc_string_clean(&fname);

    index_schema = image_spec_schema_image_index_schema_parse_file("index.json", 0, &err);
    assert_non_null(index_schema);
    ret = build_manifest_filename_from_index(index_schema, "version99", "duid3", "blobs", &fname);
    assert_true(ret);
    free_image_spec_schema_image_index_schema(index_schema);
    amxc_string_clean(&fname);

    index_schema = image_spec_schema_image_index_schema_parse_file("index.json", 0, &err);
    assert_non_null(index_schema);
    ret = build_manifest_filename_from_index(index_schema, "version99", "duid99", "blobs", &fname);
    assert_true(ret);
    free_image_spec_schema_image_index_schema(index_schema);
    amxc_string_clean(&fname);

    index_schema = image_spec_schema_image_index_schema_parse_file("bad_index.json", 0, &err);
    assert_non_null(index_schema);
    ret = build_manifest_filename_from_index(index_schema, "latest", "duid4", "blobs", &fname);
    assert_true(ret);
    assert_true(amxc_string_is_empty(&fname));
    free_image_spec_schema_image_index_schema(index_schema);
    amxc_string_clean(&fname);

    sahTraceClose();
}

void test_check_manifest_validity(UNUSED void** state) {
    image_spec_schema_image_manifest_schema* image_manifest = NULL;
    parser_error err = NULL;
    bool ret = false;
    image_manifest = image_spec_schema_image_manifest_schema_parse_file("manifest.json", NULL, &err);
    ret = check_manifest_validity(image_manifest, err);
    assert_false(ret);
    free_image_spec_schema_image_manifest_schema(image_manifest);

    image_manifest = image_spec_schema_image_manifest_schema_parse_file("bad_manifest.json", NULL, &err);
    ret = check_manifest_validity(image_manifest, err);
    assert_true(ret);
    free_image_spec_schema_image_manifest_schema(image_manifest);

    ret = check_manifest_validity(NULL, err);
    assert_true(ret);
}

void test_rlyeh_image_already_exists(UNUSED void** state) {
    bool ret = false;
    rlyeh_image_parameters_t param;
    rlyeh_image_parameters_init(&param);

    ret = rlyeh_image_already_exists(&param, ".");
    assert_false(ret);

    param.duid = strdup("duid1");
    param.version = strdup("latest");
    ret = rlyeh_image_already_exists(&param, ".");
    assert_true(ret);

    ret = rlyeh_image_already_exists(&param, "..");
    assert_false(ret);

    free(param.duid);
    param.duid = strdup("duid2");
    ret = rlyeh_image_already_exists(&param, ".");
    assert_true(ret);

    free(param.duid);
    param.duid = strdup("duid3");
    free(param.version);
    param.version = strdup("version1");
    ret = rlyeh_image_already_exists(&param, ".");
    assert_true(ret);

    free(param.version);
    param.version = strdup("version2");
    ret = rlyeh_image_already_exists(&param, ".");
    assert_true(ret);

    free(param.version);
    param.version = strdup("version99");
    ret = rlyeh_image_already_exists(&param, ".");
    assert_false(ret);

    free(param.duid);
    param.duid = strdup("duid99");
    free(param.version);
    param.version = strdup("version2");
    ret = rlyeh_image_already_exists(&param, ".");
    assert_false(ret);

    free(param.duid);
    param.duid = strdup("duid1");
    free(param.version);
    param.version = strdup("version99");
    ret = rlyeh_image_already_exists(&param, ".");
    assert_false(ret);

    rlyeh_image_parameters_clean(&param);
    rlyeh_image_parameters_clean(NULL);
}

void test_rlyeh_file_is_symlink(UNUSED void** state) {
    char* filename = tempnam("/tmp/", "rlyeh");
    char* link;
    int ret;

    int fd = open(filename, O_WRONLY | O_CREAT);
    assert_true(fd >= 0);
    close(fd);

    link = tempnam("/tmp/", "rlyeh");
    ret = symlink(filename, link);
    assert_int_equal(ret, 0);

    ret = rlyeh_file_is_symlink(filename);
    assert_int_equal(ret, 0);

    ret = rlyeh_file_is_symlink(link);
    assert_int_equal(ret, 1);

    ret = rlyeh_file_is_symlink(NULL);
    assert_int_equal(ret, -2);

    ret = rlyeh_file_is_symlink("");
    assert_int_equal(ret, -1);

    ret = rlyeh_file_is_symlink("/imaginaryfile/inamysteriousfolder");
    assert_int_equal(ret, -1);

    rlyeh_remove_file(filename);
    rlyeh_remove_file(link);

    free(filename);
    free(link);
}

void test_file_exists(UNUSED void** state) {
    char* filename = tempnam("/tmp/", "rlyeh");
    bool ret2;

    ret2 = file_exists(filename);
    assert_false(ret2);

    int fd = open(filename, O_WRONLY | O_CREAT);
    assert_true(fd >= 0);
    close(fd);

    ret2 = file_exists(filename);
    assert_true(ret2);

    ret2 = file_exists(NULL);
    assert_false(ret2);

    ret2 = file_exists("");
    assert_false(ret2);

    ret2 = file_exists("/");
    assert_true(ret2);

    free(filename);
}

void test_dir_exists(UNUSED void** state) {
    char* dirname = tempnam("/tmp/", "rlyeh");
    char* filename = tempnam("/tmp/", "rlyeh");
    bool ret2;

    ret2 = dir_exists(dirname);
    assert_false(ret2);

    ret2 = dir_exists("/");
    assert_true(ret2);

    int fd = open(filename, O_WRONLY | O_CREAT);
    assert_true(fd >= 0);
    close(fd);

    ret2 = dir_exists(filename);
    assert_false(ret2);

    ret2 = dir_exists(NULL);
    assert_false(ret2);

    ret2 = dir_exists("");
    assert_false(ret2);

    free(dirname);
    free(filename);
}

#define SHA256 "sha256"
#define PATH "BLOB"

void test_digest_to_path(UNUSED void** state) {
    const char* digest1 = SHA256 ":" PATH;
    const char* digest2 = PATH;
    char* path = NULL;

    path = digest_to_path(digest1);
    assert_true(path != NULL);
    assert_true(strcmp(path, SHA256 "/" PATH) == 0);
    free(path);

    path = digest_to_path(digest2);
    assert_true(path != NULL);
    assert_true(strcmp(path, PATH) == 0);
    free(path);

    path = digest_to_path(NULL);
    assert_true(path == NULL);

    path = digest_to_path("");
    assert_true(path == NULL);
}


void test_rlyeh_generate_uri(UNUSED void** state) {
    int status;
    rlyeh_image_parameters_t uri_elements;
    amxc_string_t complete_uri;
    amxc_string_init(&complete_uri, 0);

    rlyeh_image_parameters_init(&uri_elements);

    status = rlyeh_parse_uri("docker://library/alpine:latest", &uri_elements);
    assert_int_equal(status, 0);
    assert_null(uri_elements.server);
    rlyeh_get_server_from_file(&uri_elements);
    assert_non_null(uri_elements.server);

    status = rlyeh_generate_uri(&uri_elements, &complete_uri, NULL);
    assert_int_equal(status, 0);
    assert_non_null(uri_elements.server);
    assert_string_equal(uri_elements.server, "registry-1.docker.io");
    assert_string_equal(complete_uri.buffer, "docker://registry-1.docker.io/library/alpine:latest");

    amxc_string_clean(&complete_uri);
    amxc_string_init(&complete_uri, 0);

    // Server name should already be filled
    status = rlyeh_generate_uri(&uri_elements, &complete_uri, NULL);
    assert_int_equal(status, 0);
    assert_string_equal(uri_elements.server, "registry-1.docker.io");
    assert_string_equal(complete_uri.buffer, "docker://registry-1.docker.io/library/alpine:latest");
    free(uri_elements.server);
    uri_elements.server = NULL;

    amxc_string_clean(&complete_uri);
    amxc_string_init(&complete_uri, 0);

    status = rlyeh_generate_uri(NULL, &complete_uri, NULL);
    assert_int_not_equal(status, 0);

    status = rlyeh_generate_uri(&uri_elements, NULL, NULL);
    assert_int_not_equal(status, 0);

    amxc_string_clean(&complete_uri);

    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    uri_elements.transport = strdup("oci");
    uri_elements.image_dir = strdup("/tmp/test_rlyeh_generate_uri/images/library/alpine");
    uri_elements.version = strdup("v1");

    status = rlyeh_generate_uri(&uri_elements, &complete_uri, NULL);
    assert_int_equal(status, 0);
    assert_string_equal(complete_uri.buffer, "oci:///tmp/test_rlyeh_generate_uri/images/library/alpine:v1");

    amxc_string_clean(&complete_uri);
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    amxc_string_init(&complete_uri, 0);

    uri_elements.transport = strdup("oci");
    uri_elements.image_name = strdup("library/alpine");
    uri_elements.version = strdup("v99");
    status = rlyeh_generate_uri(&uri_elements, &complete_uri, "/tmp/test_rlyeh_generate_uri/images");
    assert_int_equal(status, 0);
    assert_string_equal(complete_uri.buffer, "oci:///tmp/test_rlyeh_generate_uri/images/library/alpine:v99");

    amxc_string_clean(&complete_uri);
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    amxc_string_init(&complete_uri, 0);

    status = rlyeh_parse_uri("docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated:3.19", &uri_elements);
    assert_int_equal(status, 0);
    status = rlyeh_generate_uri(&uri_elements, &complete_uri, NULL);
    assert_int_equal(status, 0);
    assert_string_equal(complete_uri.buffer, "docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated:3.19");


    amxc_string_clean(&complete_uri);
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    amxc_string_init(&complete_uri, 0);

    status = rlyeh_parse_uri("oci:///tmp/test_rlyeh_generate_uri/images/library/alpine:v9.9.7", &uri_elements);
    assert_int_equal(status, 0);
    status = rlyeh_generate_uri(&uri_elements, &complete_uri, NULL);
    assert_int_equal(status, 0);
    assert_string_equal(complete_uri.buffer, "oci:///tmp/test_rlyeh_generate_uri/images/library/alpine:v9.9.7");

    amxc_string_clean(&complete_uri);
    rlyeh_image_parameters_clean(&uri_elements);
}

void test_rlyeh_is_empty_dir(UNUSED void** state) {
    mkdir("/tmp/test_rlyeh_is_empty_dir", 0755);
    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_is_empty_dir"));
    assert_false(rlyeh_is_empty_dir("/tmp/"));
    assert_true(rlyeh_is_empty_dir("/tmp/dirdoesnotexist999"));
    assert_true(rlyeh_is_empty_dir(NULL));
    rmdir("/tmp/test_rlyeh_is_empty_dir");
}

void test_rlyeh_copy_file(UNUSED void** state) {
    ssize_t retval;

    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_copy_file");

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    mkdir("/tmp/test_rlyeh_copy_file", 0755);

    retval = rlyeh_copy_file("index.json", "/tmp/test_rlyeh_copy_file/index.json");
    assert_true(retval > 0);
    assert_true(file_exists("/tmp/test_rlyeh_copy_file/index.json"));
    assert_true(compareFile("index.json", "/tmp/test_rlyeh_copy_file/index.json"));
    remove("/tmp/test_rlyeh_copy_file/index.json");

    retval = rlyeh_copy_file("bad_index.json", "/tmp/test_rlyeh_copy_file/bad_index.json");
    assert_true(retval > 0);
    assert_true(file_exists("/tmp/test_rlyeh_copy_file/bad_index.json"));
    assert_true(compareFile("bad_index.json", "/tmp/test_rlyeh_copy_file/bad_index.json"));
    remove("/tmp/test_rlyeh_copy_file/bad_index.json");

    retval = rlyeh_copy_file("index.json", "/tmp/test_rlyeh_copy_file/index.json");
    assert_true(retval > 0);
    assert_true(file_exists("/tmp/test_rlyeh_copy_file/index.json"));
    assert_true(compareFile("index.json", "/tmp/test_rlyeh_copy_file/index.json"));
    remove("/tmp/test_rlyeh_copy_file/index.json");

    retval = rlyeh_copy_file("imaginaryfile", "/tmp/test_rlyeh_copy_file/imaginaryfile");
    assert_false(retval > 0);
    assert_false(file_exists("/tmp/test_rlyeh_copy_file/imaginaryfile"));
    assert_false(file_exists("imaginaryfile"));

    retval = rlyeh_copy_file("index.json", "/tmp/test_rlyeh_copy_file/imaginarydir/index.json");
    assert_false(retval > 0);
    assert_false(file_exists("/tmp/test_rlyeh_copy_file/imaginarydir/index.json"));

    assert_true(rlyeh_is_empty_dir("/tmp/test_rlyeh_copy_file"));
    rmdir("/tmp/test_rlyeh_copy_file");

    sahTraceClose();
}

void test_rlyeh_move_file(UNUSED void** state) {
    int retval;

    remove("indexcopy.json");
    rlyeh_remove_dir_unsafe("/tmp/test_rlyeh_move_file");

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    mkdir("/tmp/test_rlyeh_move_file", 0755);

    assert_true(rlyeh_copy_file("index.json", "indexcopy.json") >= 0);

    retval = rlyeh_move_file("indexcopy.json", "/tmp/test_rlyeh_move_file/indexcopy.json");
    assert_true(retval >= 0);
    assert_true(file_exists("/tmp/test_rlyeh_move_file/indexcopy.json"));
    assert_false(file_exists("indexcopy.json"));
    assert_true(compareFile("index.json", "/tmp/test_rlyeh_move_file/indexcopy.json"));

    retval = rlyeh_move_file("/tmp/test_rlyeh_move_file/indexcopy.json", "/tmp/test_rlyeh_move_file/indexcopy2.json");
    assert_true(retval >= 0);
    assert_true(file_exists("/tmp/test_rlyeh_move_file/indexcopy2.json"));
    assert_false(file_exists("/tmp/test_rlyeh_move_file/indexcopy.json"));
    assert_true(compareFile("index.json", "/tmp/test_rlyeh_move_file/indexcopy2.json"));

    remove("indexcopy.json");
    rmdir("/tmp/test_rlyeh_move_file");

    sahTraceClose();
}
