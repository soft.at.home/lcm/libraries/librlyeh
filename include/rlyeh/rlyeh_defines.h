/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _RLYEH_DEFINES_H_
#define _RLYEH_DEFINES_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define RLYEH "Rlyeh"
#define RLYEH_IMAGES "Images"
#define RLYEH_ONBOARDING_FILE "OnboardingFile"
#define RLYEH_STORAGE_LOCATION "StorageLocation"
#define RLYEH_STORAGE_LOCATION_RO "RO" RLYEH_STORAGE_LOCATION
#define RLYEH_IMAGE_LOCATION "ImageLocation"
#define RLYEH_IMAGE_LOCATION_RO "RO" RLYEH_IMAGE_LOCATION
#define RLYEH_SIGNATURE_VERIFICATION "SignatureVerification"
#define RLYEH_CERTIFICATE_VERIFICATION "CertificateVerification"
#define RLYEH_REMAINING_DISKSPACE_BYTES "RemainingDiskSpaceBytes"
#define RLYEH_PLUGIN_LOCATION "PluginLocation"

#define RLYEH_PLUGIN "RlyehPlugin"
//hooks
#define RLYEH_PLUGIN_INIT "RlyehPluginInit"
#define RLYEH_PLUGIN_CLEANUP "RlyehPluginCleanup"
#define RLYEH_PLUGIN_IMAGE_AUTHENTICATION "RlyehPluginImageAuthentication"
//Arguments
#define RLYEH_PLUGIN_ARG_DATAMODEL "PluginArgDatamodel"
#define RLYEH_PLUGIN_ARG_URL "PluginArgUrl"
#define RLYEH_PLUGIN_ARG_MANIFEST_HASH "PluginArgManifestHash"
#define RLYEH_PLUGIN_ARG_IMAGE_NAME "PluginArgImageName"
#define RLYEH_PLUGIN_ARG_SERVER "PluginArgServer"
#define RLYEH_PLUGIN_ARG_VERSION "PluginArgVersion"


#define RLYEH_DM RLYEH
#define RLYEH_DM_IMAGES RLYEH_DM "." RLYEH_IMAGES
#define RLYEH_DM_STORAGE_LOCATION RLYEH_DM "." RLYEH_STORAGE_LOCATION
#define RLYEH_DM_IMAGE_LOCATION RLYEH_DM "." RLYEH_IMAGE_LOCATION
#define RLYEH_DM_SIGNATURE_VERIFICATION RLYEH_DM "." RLYEH_SIGNATURE_VERIFICATION
#define RLYEH_DM_REMAINING_DISKSPACE_BYTES RLYEH_DM "." RLYEH_REMAINING_DISKSPACE_BYTES

#define RLYEH_CMD_PULL_URI  "URI"
#define RLYEH_CMD_PULL_UUID "UUID"
#define RLYEH_CMD_PULL_DUID "DUID"
#define RLYEH_CMD_PULL_USERNAME "username"
#define RLYEH_CMD_PULL_PASSWORD "password"
#define RLYEH_CMD_PULL_SIGNATURE_URL "signature"
#define RLYEH_CMD_PULL_VERSION "Version"
#define RLYEH_CMD_PULL_IMAGE_DISKLOCATION "DiskLocation"


#define RLYEH_DM_IMAGE_UUID RLYEH_CMD_PULL_UUID
#define RLYEH_DM_IMAGE_DUID RLYEH_CMD_PULL_DUID
#define RLYEH_DM_IMAGE_URI RLYEH_CMD_PULL_URI
#define RLYEH_DM_IMAGE_NAME "Name"
#define RLYEH_DM_IMAGE_DISKLOCATION RLYEH_CMD_PULL_IMAGE_DISKLOCATION
#define RLYEH_DM_IMAGE_VERSION RLYEH_CMD_PULL_VERSION
#define RLYEH_DM_IMAGE_VENDOR "Vendor"
#define RLYEH_DM_IMAGE_DESCRIPTION "Description"
#define RLYEH_DM_IMAGE_MARK_RM "MarkForRemoval"
#define RLYEH_DM_IMAGE_STATUS "Status"
#define RLYEH_DM_IMAGE_ERROR_CODE "ErrorCode"

#define RLYEH_COMMAND_PARAMETERS "parameters"
#define RLYEH_COMMAND "command"

#define RLYEH_STATUS_DOWNLOADED "Downloaded"
#define RLYEH_STATUS_DOWNLOADING "Downloading"
#define RLYEH_STATUS_DOWNLOAD_FAILED "DownloadFailed"
#define RLYEH_STATUS_UNENCRYPTION_FAILED "UnencryptionFailed"

#define RLYEH_NOTIF_ERROR_TYPE "type"
#define RLYEH_NOTIF_ERROR_TYPESTR "str"
#define RLYEH_NOTIF_ERROR_COMMAND RLYEH_COMMAND
#define RLYEH_NOTIF_ERROR_REASON "reason"

#define RLYEH_NOTIF_CONTAINER_ID "containerID"
#define RLYEH_NOTIF_COMMAND_ID "commandID"
#define RLYEH_NOTIF_PARAMETERS "parameters"
#define RLYEH_NOTIF_IMAGES "images"

#define RLYEH_NOTIF_IMAGE_PULLED "rlyeh:image:pulled"
#define RLYEH_NOTIF_IMAGE_MARKED_RM "rlyeh:image:markedremove"
#define RLYEH_NOTIF_IMAGE_REMOVED "rlyeh:image:removed"
#define RLYEH_NOTIF_IMAGE_STATUS "rlyeh:image:status"
#define RLYEH_NOTIF_IMAGE_LIST "rlyeh:image:list"
#define RLYEH_NOTIF_ERROR "rlyeh:error"

#define RLYEH_CMD_RM_PARAMKEY "paramKey"
#define RLYEH_CMD_RM_PARAMVALUE "paramValue"

#define RLYEH_CMD_PULL "pull"
#define RLYEH_CMD_REMOVE "remove"
#define RLYEH_CMD_GC "gc"

#define RLYEH_CMD_RES "res"

#define RLYEH_CMD_SV "sv"
#define RLYEH_CMD_SV_ENABLE "Enable"

#define CUSTOM_BASE "com.softathome."
#define CUSTOM_BASE_IMAGE CUSTOM_BASE "image."

#define OCI_BASE "org.opencontainers."
#define OCI_BASE_IMAGE OCI_BASE "image."

#define RLYEH_ANNOTATION_LCM_UUID CUSTOM_BASE_IMAGE "uuid"
#define RLYEH_ANNOTATION_LCM_DUID CUSTOM_BASE_IMAGE "duid"
#define RLYEH_ANNOTATION_LCM_URI CUSTOM_BASE_IMAGE "uri"
#define RLYEH_ANNOTATION_LCM_MAKRFORREMOVAL CUSTOM_BASE_IMAGE  "markforremoval"

#define RLYEH_ANNOTATION_OCI_IMAGE_REFNAME OCI_BASE_IMAGE "ref.name"
#define RLYEH_ANNOTATION_OCI_IMAGE_VERSION OCI_BASE_IMAGE "version"
#define RLYEH_ANNOTATION_OCI_IMAGE_VENDOR OCI_BASE_IMAGE "vendor"
#define RLYEH_ANNOTATION_OCI_IMAGE_TITLE OCI_BASE_IMAGE "title"
#define RLYEH_ANNOTATION_OCI_IMAGE_DESCRIPTION OCI_BASE_IMAGE "description"

#ifdef __cplusplus
}
#endif

#endif // _RLYEH_DEFINES_H_
